# Install script for directory: /opt/opencog/lib

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Debug")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CPACK_ABSOLUTE_DESTINATION_FILES
   "/usr/local/etc/opencog.conf;/usr/local/etc/opencog-chatbot.conf;/usr/local/etc/opencog-test.conf.example")
FILE(INSTALL DESTINATION "/usr/local/etc" TYPE FILE FILES
    "/opt/opencog/lib/opencog.conf"
    "/opt/opencog/lib/opencog-chatbot.conf"
    "/opt/opencog/lib/opencog-test.conf.example"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CPACK_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/opencog/cmake/FindCxxtest.cmake;/usr/local/share/opencog/cmake/FindGuile.cmake;/usr/local/share/opencog/cmake/FindGSL.cmake;/usr/local/share/opencog/cmake/FindPthreads.cmake;/usr/local/share/opencog/cmake/FindSTLPort.cmake;/usr/local/share/opencog/cmake/FindUnixODBC.cmake;/usr/local/share/opencog/cmake/FindLuabind.cmake;/usr/local/share/opencog/cmake/FindXercesC.cmake;/usr/local/share/opencog/cmake/OpenCogAtomTypes.cmake;/usr/local/share/opencog/cmake/OpenCogConfig.cmake")
FILE(INSTALL DESTINATION "/usr/local/share/opencog/cmake" TYPE FILE FILES
    "/opt/opencog/lib/FindCxxtest.cmake"
    "/opt/opencog/lib/FindGuile.cmake"
    "/opt/opencog/lib/FindGSL.cmake"
    "/opt/opencog/lib/FindPthreads.cmake"
    "/opt/opencog/lib/FindSTLPort.cmake"
    "/opt/opencog/lib/FindUnixODBC.cmake"
    "/opt/opencog/lib/FindLuabind.cmake"
    "/opt/opencog/lib/FindXercesC.cmake"
    "/opt/opencog/lib/OpenCogAtomTypes.cmake"
    "/opt/opencog/lib/OpenCogConfig.cmake"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

