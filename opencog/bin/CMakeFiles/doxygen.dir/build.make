# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canoncical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /opt/opencog

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /opt/opencog/bin

# Utility rule file for doxygen.

CMakeFiles/doxygen:
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold "Generating documentation"
	/usr/bin/doxygen /opt/opencog/bin/Doxyfile

doxygen: CMakeFiles/doxygen
doxygen: CMakeFiles/doxygen.dir/build.make
.PHONY : doxygen

# Rule to build all files generated by this target.
CMakeFiles/doxygen.dir/build: doxygen
.PHONY : CMakeFiles/doxygen.dir/build

CMakeFiles/doxygen.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/doxygen.dir/cmake_clean.cmake
.PHONY : CMakeFiles/doxygen.dir/clean

CMakeFiles/doxygen.dir/depend:
	cd /opt/opencog/bin && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /opt/opencog /opt/opencog /opt/opencog/bin /opt/opencog/bin /opt/opencog/bin/CMakeFiles/doxygen.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/doxygen.dir/depend

