# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/examples/hopfield/HopfieldDemo.cc" "/opt/opencog/bin/examples/hopfield/CMakeFiles/hopfield.dir/HopfieldDemo.cc.o"
  "/opt/opencog/examples/hopfield/HopfieldOptions.cc" "/opt/opencog/bin/examples/hopfield/CMakeFiles/hopfield.dir/HopfieldOptions.cc.o"
  "/opt/opencog/examples/hopfield/HopfieldServer.cc" "/opt/opencog/bin/examples/hopfield/CMakeFiles/hopfield.dir/HopfieldServer.cc.o"
  "/opt/opencog/examples/hopfield/HopfieldUbigrapher.cc" "/opt/opencog/bin/examples/hopfield/CMakeFiles/hopfield.dir/HopfieldUbigrapher.cc.o"
  "/opt/opencog/examples/hopfield/ImprintAgent.cc" "/opt/opencog/bin/examples/hopfield/CMakeFiles/hopfield.dir/ImprintAgent.cc.o"
  "/opt/opencog/examples/hopfield/Pattern.cc" "/opt/opencog/bin/examples/hopfield/CMakeFiles/hopfield.dir/Pattern.cc.o"
  "/opt/opencog/examples/hopfield/StorkeyAgent.cc" "/opt/opencog/bin/examples/hopfield/CMakeFiles/hopfield.dir/StorkeyAgent.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  "NDEBUG"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opt/opencog/bin/opencog/server/CMakeFiles/server.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/dynamics/attention/CMakeFiles/attention.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/ubigraph/CMakeFiles/ubigraph.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/persist/sql/CMakeFiles/persist.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/guile/CMakeFiles/smob.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/persist/xml/CMakeFiles/xml.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/DependInfo.cmake"
  )
