# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/opt/opencog/opencog/util/cluster.c" "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/cluster.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/util/Config.cc" "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/Config.cc.o"
  "/opt/opencog/opencog/util/Logger.cc" "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/Logger.cc.o"
  "/opt/opencog/opencog/util/StringManipulator.cc" "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/StringManipulator.cc.o"
  "/opt/opencog/opencog/util/StringTokenizer.cc" "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/StringTokenizer.cc.o"
  "/opt/opencog/opencog/util/ansi.cc" "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/ansi.cc.o"
  "/opt/opencog/opencog/util/exceptions.cc" "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/exceptions.cc.o"
  "/opt/opencog/opencog/util/files.cc" "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/files.cc.o"
  "/opt/opencog/opencog/util/lazy_normal_selector.cc" "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/lazy_normal_selector.cc.o"
  "/opt/opencog/opencog/util/lazy_random_selector.cc" "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/lazy_random_selector.cc.o"
  "/opt/opencog/opencog/util/lazy_selector.cc" "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/lazy_selector.cc.o"
  "/opt/opencog/opencog/util/log_prog_name.cc" "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/log_prog_name.cc.o"
  "/opt/opencog/opencog/util/misc.cc" "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/misc.cc.o"
  "/opt/opencog/opencog/util/mt19937ar.cc" "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/mt19937ar.cc.o"
  "/opt/opencog/opencog/util/oc_assert.cc" "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/oc_assert.cc.o"
  "/opt/opencog/opencog/util/oc_omp.cc" "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/oc_omp.cc.o"
  "/opt/opencog/opencog/util/octime.cc" "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/octime.cc.o"
  "/opt/opencog/opencog/util/platform.cc" "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/platform.cc.o"
  "/opt/opencog/opencog/util/tree.cc" "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/tree.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )
