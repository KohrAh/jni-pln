# Install script for directory: /opt/opencog/opencog/util

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Debug")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/opencog/util" TYPE FILE FILES
    "/opt/opencog/opencog/util/ansi.h"
    "/opt/opencog/opencog/util/algorithm.h"
    "/opt/opencog/opencog/util/log_prog_name.h"
    "/opt/opencog/opencog/util/based_variant.h"
    "/opt/opencog/opencog/util/cluster.h"
    "/opt/opencog/opencog/util/Config.h"
    "/opt/opencog/opencog/util/Cover_Tree.h"
    "/opt/opencog/opencog/util/concurrent_queue.h"
    "/opt/opencog/opencog/util/digraph.h"
    "/opt/opencog/opencog/util/dorepeat.h"
    "/opt/opencog/opencog/util/exceptions.h"
    "/opt/opencog/opencog/util/files.h"
    "/opt/opencog/opencog/util/foreach.h"
    "/opt/opencog/opencog/util/functional.h"
    "/opt/opencog/opencog/util/oc_assert.h"
    "/opt/opencog/opencog/util/hashing.h"
    "/opt/opencog/opencog/util/iostreamContainer.h"
    "/opt/opencog/opencog/util/lazy_normal_selector.h"
    "/opt/opencog/opencog/util/lazy_random_selector.h"
    "/opt/opencog/opencog/util/lazy_selector.h"
    "/opt/opencog/opencog/util/Logger.h"
    "/opt/opencog/opencog/util/lru_cache.h"
    "/opt/opencog/opencog/util/misc.h"
    "/opt/opencog/opencog/util/mt19937ar.h"
    "/opt/opencog/opencog/util/random.h"
    "/opt/opencog/opencog/util/numeric.h"
    "/opt/opencog/opencog/util/platform.h"
    "/opt/opencog/opencog/util/RandGen.h"
    "/opt/opencog/opencog/util/recent_val.h"
    "/opt/opencog/opencog/util/selection.h"
    "/opt/opencog/opencog/util/StringManipulator.h"
    "/opt/opencog/opencog/util/StringTokenizer.h"
    "/opt/opencog/opencog/util/tree.h"
    "/opt/opencog/opencog/util/octime.h"
    "/opt/opencog/opencog/util/oc_omp.h"
    "/opt/opencog/opencog/util/Counter.h"
    "/opt/opencog/opencog/util/KLD.h"
    "/opt/opencog/opencog/util/comprehension.h"
    "/opt/opencog/opencog/util/ranking.h"
    "/opt/opencog/opencog/util/MannWhitneyU.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libutil.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libutil.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libutil.so"
         RPATH "/usr/local/lib/opencog")
  ENDIF()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/opencog" TYPE SHARED_LIBRARY FILES "/opt/opencog/bin/opencog/util/libutil.so")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libutil.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libutil.so")
    FILE(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libutil.so"
         OLD_RPATH "::::::::::::::::::::::"
         NEW_RPATH "/usr/local/lib/opencog")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libutil.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

