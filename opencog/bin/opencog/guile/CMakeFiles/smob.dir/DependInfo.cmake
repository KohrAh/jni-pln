# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/guile/SchemeEval.cc" "/opt/opencog/bin/opencog/guile/CMakeFiles/smob.dir/SchemeEval.cc.o"
  "/opt/opencog/opencog/guile/SchemeExec.cc" "/opt/opencog/bin/opencog/guile/CMakeFiles/smob.dir/SchemeExec.cc.o"
  "/opt/opencog/opencog/guile/SchemePrimitive.cc" "/opt/opencog/bin/opencog/guile/CMakeFiles/smob.dir/SchemePrimitive.cc.o"
  "/opt/opencog/opencog/guile/SchemeSmob.cc" "/opt/opencog/bin/opencog/guile/CMakeFiles/smob.dir/SchemeSmob.cc.o"
  "/opt/opencog/opencog/guile/SchemeSmobAV.cc" "/opt/opencog/bin/opencog/guile/CMakeFiles/smob.dir/SchemeSmobAV.cc.o"
  "/opt/opencog/opencog/guile/SchemeSmobAtom.cc" "/opt/opencog/bin/opencog/guile/CMakeFiles/smob.dir/SchemeSmobAtom.cc.o"
  "/opt/opencog/opencog/guile/SchemeSmobGC.cc" "/opt/opencog/bin/opencog/guile/CMakeFiles/smob.dir/SchemeSmobGC.cc.o"
  "/opt/opencog/opencog/guile/SchemeSmobNew.cc" "/opt/opencog/bin/opencog/guile/CMakeFiles/smob.dir/SchemeSmobNew.cc.o"
  "/opt/opencog/opencog/guile/SchemeSmobTV.cc" "/opt/opencog/bin/opencog/guile/CMakeFiles/smob.dir/SchemeSmobTV.cc.o"
  "/opt/opencog/opencog/guile/SchemeSmobVH.cc" "/opt/opencog/bin/opencog/guile/CMakeFiles/smob.dir/SchemeSmobVH.cc.o"
  "/opt/opencog/opencog/guile/load-file.cc" "/opt/opencog/bin/opencog/guile/CMakeFiles/smob.dir/load-file.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )
