# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canoncical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /opt/opencog

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /opt/opencog/bin

# Include any dependencies generated for this target.
include opencog/guile/CMakeFiles/smob.dir/depend.make

# Include the progress variables for this target.
include opencog/guile/CMakeFiles/smob.dir/progress.make

# Include the compile flags for this target's objects.
include opencog/guile/CMakeFiles/smob.dir/flags.make

opencog/guile/CMakeFiles/smob.dir/SchemeEval.cc.o: opencog/guile/CMakeFiles/smob.dir/flags.make
opencog/guile/CMakeFiles/smob.dir/SchemeEval.cc.o: ../opencog/guile/SchemeEval.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/guile/CMakeFiles/smob.dir/SchemeEval.cc.o"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/smob.dir/SchemeEval.cc.o -c /opt/opencog/opencog/guile/SchemeEval.cc

opencog/guile/CMakeFiles/smob.dir/SchemeEval.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/smob.dir/SchemeEval.cc.i"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/guile/SchemeEval.cc > CMakeFiles/smob.dir/SchemeEval.cc.i

opencog/guile/CMakeFiles/smob.dir/SchemeEval.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/smob.dir/SchemeEval.cc.s"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/guile/SchemeEval.cc -o CMakeFiles/smob.dir/SchemeEval.cc.s

opencog/guile/CMakeFiles/smob.dir/SchemeEval.cc.o.requires:
.PHONY : opencog/guile/CMakeFiles/smob.dir/SchemeEval.cc.o.requires

opencog/guile/CMakeFiles/smob.dir/SchemeEval.cc.o.provides: opencog/guile/CMakeFiles/smob.dir/SchemeEval.cc.o.requires
	$(MAKE) -f opencog/guile/CMakeFiles/smob.dir/build.make opencog/guile/CMakeFiles/smob.dir/SchemeEval.cc.o.provides.build
.PHONY : opencog/guile/CMakeFiles/smob.dir/SchemeEval.cc.o.provides

opencog/guile/CMakeFiles/smob.dir/SchemeEval.cc.o.provides.build: opencog/guile/CMakeFiles/smob.dir/SchemeEval.cc.o

opencog/guile/CMakeFiles/smob.dir/SchemeExec.cc.o: opencog/guile/CMakeFiles/smob.dir/flags.make
opencog/guile/CMakeFiles/smob.dir/SchemeExec.cc.o: ../opencog/guile/SchemeExec.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/guile/CMakeFiles/smob.dir/SchemeExec.cc.o"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/smob.dir/SchemeExec.cc.o -c /opt/opencog/opencog/guile/SchemeExec.cc

opencog/guile/CMakeFiles/smob.dir/SchemeExec.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/smob.dir/SchemeExec.cc.i"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/guile/SchemeExec.cc > CMakeFiles/smob.dir/SchemeExec.cc.i

opencog/guile/CMakeFiles/smob.dir/SchemeExec.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/smob.dir/SchemeExec.cc.s"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/guile/SchemeExec.cc -o CMakeFiles/smob.dir/SchemeExec.cc.s

opencog/guile/CMakeFiles/smob.dir/SchemeExec.cc.o.requires:
.PHONY : opencog/guile/CMakeFiles/smob.dir/SchemeExec.cc.o.requires

opencog/guile/CMakeFiles/smob.dir/SchemeExec.cc.o.provides: opencog/guile/CMakeFiles/smob.dir/SchemeExec.cc.o.requires
	$(MAKE) -f opencog/guile/CMakeFiles/smob.dir/build.make opencog/guile/CMakeFiles/smob.dir/SchemeExec.cc.o.provides.build
.PHONY : opencog/guile/CMakeFiles/smob.dir/SchemeExec.cc.o.provides

opencog/guile/CMakeFiles/smob.dir/SchemeExec.cc.o.provides.build: opencog/guile/CMakeFiles/smob.dir/SchemeExec.cc.o

opencog/guile/CMakeFiles/smob.dir/SchemePrimitive.cc.o: opencog/guile/CMakeFiles/smob.dir/flags.make
opencog/guile/CMakeFiles/smob.dir/SchemePrimitive.cc.o: ../opencog/guile/SchemePrimitive.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_3)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/guile/CMakeFiles/smob.dir/SchemePrimitive.cc.o"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/smob.dir/SchemePrimitive.cc.o -c /opt/opencog/opencog/guile/SchemePrimitive.cc

opencog/guile/CMakeFiles/smob.dir/SchemePrimitive.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/smob.dir/SchemePrimitive.cc.i"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/guile/SchemePrimitive.cc > CMakeFiles/smob.dir/SchemePrimitive.cc.i

opencog/guile/CMakeFiles/smob.dir/SchemePrimitive.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/smob.dir/SchemePrimitive.cc.s"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/guile/SchemePrimitive.cc -o CMakeFiles/smob.dir/SchemePrimitive.cc.s

opencog/guile/CMakeFiles/smob.dir/SchemePrimitive.cc.o.requires:
.PHONY : opencog/guile/CMakeFiles/smob.dir/SchemePrimitive.cc.o.requires

opencog/guile/CMakeFiles/smob.dir/SchemePrimitive.cc.o.provides: opencog/guile/CMakeFiles/smob.dir/SchemePrimitive.cc.o.requires
	$(MAKE) -f opencog/guile/CMakeFiles/smob.dir/build.make opencog/guile/CMakeFiles/smob.dir/SchemePrimitive.cc.o.provides.build
.PHONY : opencog/guile/CMakeFiles/smob.dir/SchemePrimitive.cc.o.provides

opencog/guile/CMakeFiles/smob.dir/SchemePrimitive.cc.o.provides.build: opencog/guile/CMakeFiles/smob.dir/SchemePrimitive.cc.o

opencog/guile/CMakeFiles/smob.dir/SchemeSmob.cc.o: opencog/guile/CMakeFiles/smob.dir/flags.make
opencog/guile/CMakeFiles/smob.dir/SchemeSmob.cc.o: ../opencog/guile/SchemeSmob.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_4)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/guile/CMakeFiles/smob.dir/SchemeSmob.cc.o"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/smob.dir/SchemeSmob.cc.o -c /opt/opencog/opencog/guile/SchemeSmob.cc

opencog/guile/CMakeFiles/smob.dir/SchemeSmob.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/smob.dir/SchemeSmob.cc.i"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/guile/SchemeSmob.cc > CMakeFiles/smob.dir/SchemeSmob.cc.i

opencog/guile/CMakeFiles/smob.dir/SchemeSmob.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/smob.dir/SchemeSmob.cc.s"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/guile/SchemeSmob.cc -o CMakeFiles/smob.dir/SchemeSmob.cc.s

opencog/guile/CMakeFiles/smob.dir/SchemeSmob.cc.o.requires:
.PHONY : opencog/guile/CMakeFiles/smob.dir/SchemeSmob.cc.o.requires

opencog/guile/CMakeFiles/smob.dir/SchemeSmob.cc.o.provides: opencog/guile/CMakeFiles/smob.dir/SchemeSmob.cc.o.requires
	$(MAKE) -f opencog/guile/CMakeFiles/smob.dir/build.make opencog/guile/CMakeFiles/smob.dir/SchemeSmob.cc.o.provides.build
.PHONY : opencog/guile/CMakeFiles/smob.dir/SchemeSmob.cc.o.provides

opencog/guile/CMakeFiles/smob.dir/SchemeSmob.cc.o.provides.build: opencog/guile/CMakeFiles/smob.dir/SchemeSmob.cc.o

opencog/guile/CMakeFiles/smob.dir/SchemeSmobAtom.cc.o: opencog/guile/CMakeFiles/smob.dir/flags.make
opencog/guile/CMakeFiles/smob.dir/SchemeSmobAtom.cc.o: ../opencog/guile/SchemeSmobAtom.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_5)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/guile/CMakeFiles/smob.dir/SchemeSmobAtom.cc.o"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/smob.dir/SchemeSmobAtom.cc.o -c /opt/opencog/opencog/guile/SchemeSmobAtom.cc

opencog/guile/CMakeFiles/smob.dir/SchemeSmobAtom.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/smob.dir/SchemeSmobAtom.cc.i"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/guile/SchemeSmobAtom.cc > CMakeFiles/smob.dir/SchemeSmobAtom.cc.i

opencog/guile/CMakeFiles/smob.dir/SchemeSmobAtom.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/smob.dir/SchemeSmobAtom.cc.s"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/guile/SchemeSmobAtom.cc -o CMakeFiles/smob.dir/SchemeSmobAtom.cc.s

opencog/guile/CMakeFiles/smob.dir/SchemeSmobAtom.cc.o.requires:
.PHONY : opencog/guile/CMakeFiles/smob.dir/SchemeSmobAtom.cc.o.requires

opencog/guile/CMakeFiles/smob.dir/SchemeSmobAtom.cc.o.provides: opencog/guile/CMakeFiles/smob.dir/SchemeSmobAtom.cc.o.requires
	$(MAKE) -f opencog/guile/CMakeFiles/smob.dir/build.make opencog/guile/CMakeFiles/smob.dir/SchemeSmobAtom.cc.o.provides.build
.PHONY : opencog/guile/CMakeFiles/smob.dir/SchemeSmobAtom.cc.o.provides

opencog/guile/CMakeFiles/smob.dir/SchemeSmobAtom.cc.o.provides.build: opencog/guile/CMakeFiles/smob.dir/SchemeSmobAtom.cc.o

opencog/guile/CMakeFiles/smob.dir/SchemeSmobAV.cc.o: opencog/guile/CMakeFiles/smob.dir/flags.make
opencog/guile/CMakeFiles/smob.dir/SchemeSmobAV.cc.o: ../opencog/guile/SchemeSmobAV.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_6)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/guile/CMakeFiles/smob.dir/SchemeSmobAV.cc.o"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/smob.dir/SchemeSmobAV.cc.o -c /opt/opencog/opencog/guile/SchemeSmobAV.cc

opencog/guile/CMakeFiles/smob.dir/SchemeSmobAV.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/smob.dir/SchemeSmobAV.cc.i"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/guile/SchemeSmobAV.cc > CMakeFiles/smob.dir/SchemeSmobAV.cc.i

opencog/guile/CMakeFiles/smob.dir/SchemeSmobAV.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/smob.dir/SchemeSmobAV.cc.s"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/guile/SchemeSmobAV.cc -o CMakeFiles/smob.dir/SchemeSmobAV.cc.s

opencog/guile/CMakeFiles/smob.dir/SchemeSmobAV.cc.o.requires:
.PHONY : opencog/guile/CMakeFiles/smob.dir/SchemeSmobAV.cc.o.requires

opencog/guile/CMakeFiles/smob.dir/SchemeSmobAV.cc.o.provides: opencog/guile/CMakeFiles/smob.dir/SchemeSmobAV.cc.o.requires
	$(MAKE) -f opencog/guile/CMakeFiles/smob.dir/build.make opencog/guile/CMakeFiles/smob.dir/SchemeSmobAV.cc.o.provides.build
.PHONY : opencog/guile/CMakeFiles/smob.dir/SchemeSmobAV.cc.o.provides

opencog/guile/CMakeFiles/smob.dir/SchemeSmobAV.cc.o.provides.build: opencog/guile/CMakeFiles/smob.dir/SchemeSmobAV.cc.o

opencog/guile/CMakeFiles/smob.dir/SchemeSmobGC.cc.o: opencog/guile/CMakeFiles/smob.dir/flags.make
opencog/guile/CMakeFiles/smob.dir/SchemeSmobGC.cc.o: ../opencog/guile/SchemeSmobGC.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_7)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/guile/CMakeFiles/smob.dir/SchemeSmobGC.cc.o"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/smob.dir/SchemeSmobGC.cc.o -c /opt/opencog/opencog/guile/SchemeSmobGC.cc

opencog/guile/CMakeFiles/smob.dir/SchemeSmobGC.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/smob.dir/SchemeSmobGC.cc.i"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/guile/SchemeSmobGC.cc > CMakeFiles/smob.dir/SchemeSmobGC.cc.i

opencog/guile/CMakeFiles/smob.dir/SchemeSmobGC.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/smob.dir/SchemeSmobGC.cc.s"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/guile/SchemeSmobGC.cc -o CMakeFiles/smob.dir/SchemeSmobGC.cc.s

opencog/guile/CMakeFiles/smob.dir/SchemeSmobGC.cc.o.requires:
.PHONY : opencog/guile/CMakeFiles/smob.dir/SchemeSmobGC.cc.o.requires

opencog/guile/CMakeFiles/smob.dir/SchemeSmobGC.cc.o.provides: opencog/guile/CMakeFiles/smob.dir/SchemeSmobGC.cc.o.requires
	$(MAKE) -f opencog/guile/CMakeFiles/smob.dir/build.make opencog/guile/CMakeFiles/smob.dir/SchemeSmobGC.cc.o.provides.build
.PHONY : opencog/guile/CMakeFiles/smob.dir/SchemeSmobGC.cc.o.provides

opencog/guile/CMakeFiles/smob.dir/SchemeSmobGC.cc.o.provides.build: opencog/guile/CMakeFiles/smob.dir/SchemeSmobGC.cc.o

opencog/guile/CMakeFiles/smob.dir/SchemeSmobNew.cc.o: opencog/guile/CMakeFiles/smob.dir/flags.make
opencog/guile/CMakeFiles/smob.dir/SchemeSmobNew.cc.o: ../opencog/guile/SchemeSmobNew.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_8)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/guile/CMakeFiles/smob.dir/SchemeSmobNew.cc.o"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/smob.dir/SchemeSmobNew.cc.o -c /opt/opencog/opencog/guile/SchemeSmobNew.cc

opencog/guile/CMakeFiles/smob.dir/SchemeSmobNew.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/smob.dir/SchemeSmobNew.cc.i"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/guile/SchemeSmobNew.cc > CMakeFiles/smob.dir/SchemeSmobNew.cc.i

opencog/guile/CMakeFiles/smob.dir/SchemeSmobNew.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/smob.dir/SchemeSmobNew.cc.s"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/guile/SchemeSmobNew.cc -o CMakeFiles/smob.dir/SchemeSmobNew.cc.s

opencog/guile/CMakeFiles/smob.dir/SchemeSmobNew.cc.o.requires:
.PHONY : opencog/guile/CMakeFiles/smob.dir/SchemeSmobNew.cc.o.requires

opencog/guile/CMakeFiles/smob.dir/SchemeSmobNew.cc.o.provides: opencog/guile/CMakeFiles/smob.dir/SchemeSmobNew.cc.o.requires
	$(MAKE) -f opencog/guile/CMakeFiles/smob.dir/build.make opencog/guile/CMakeFiles/smob.dir/SchemeSmobNew.cc.o.provides.build
.PHONY : opencog/guile/CMakeFiles/smob.dir/SchemeSmobNew.cc.o.provides

opencog/guile/CMakeFiles/smob.dir/SchemeSmobNew.cc.o.provides.build: opencog/guile/CMakeFiles/smob.dir/SchemeSmobNew.cc.o

opencog/guile/CMakeFiles/smob.dir/SchemeSmobTV.cc.o: opencog/guile/CMakeFiles/smob.dir/flags.make
opencog/guile/CMakeFiles/smob.dir/SchemeSmobTV.cc.o: ../opencog/guile/SchemeSmobTV.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_9)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/guile/CMakeFiles/smob.dir/SchemeSmobTV.cc.o"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/smob.dir/SchemeSmobTV.cc.o -c /opt/opencog/opencog/guile/SchemeSmobTV.cc

opencog/guile/CMakeFiles/smob.dir/SchemeSmobTV.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/smob.dir/SchemeSmobTV.cc.i"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/guile/SchemeSmobTV.cc > CMakeFiles/smob.dir/SchemeSmobTV.cc.i

opencog/guile/CMakeFiles/smob.dir/SchemeSmobTV.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/smob.dir/SchemeSmobTV.cc.s"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/guile/SchemeSmobTV.cc -o CMakeFiles/smob.dir/SchemeSmobTV.cc.s

opencog/guile/CMakeFiles/smob.dir/SchemeSmobTV.cc.o.requires:
.PHONY : opencog/guile/CMakeFiles/smob.dir/SchemeSmobTV.cc.o.requires

opencog/guile/CMakeFiles/smob.dir/SchemeSmobTV.cc.o.provides: opencog/guile/CMakeFiles/smob.dir/SchemeSmobTV.cc.o.requires
	$(MAKE) -f opencog/guile/CMakeFiles/smob.dir/build.make opencog/guile/CMakeFiles/smob.dir/SchemeSmobTV.cc.o.provides.build
.PHONY : opencog/guile/CMakeFiles/smob.dir/SchemeSmobTV.cc.o.provides

opencog/guile/CMakeFiles/smob.dir/SchemeSmobTV.cc.o.provides.build: opencog/guile/CMakeFiles/smob.dir/SchemeSmobTV.cc.o

opencog/guile/CMakeFiles/smob.dir/SchemeSmobVH.cc.o: opencog/guile/CMakeFiles/smob.dir/flags.make
opencog/guile/CMakeFiles/smob.dir/SchemeSmobVH.cc.o: ../opencog/guile/SchemeSmobVH.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_10)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/guile/CMakeFiles/smob.dir/SchemeSmobVH.cc.o"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/smob.dir/SchemeSmobVH.cc.o -c /opt/opencog/opencog/guile/SchemeSmobVH.cc

opencog/guile/CMakeFiles/smob.dir/SchemeSmobVH.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/smob.dir/SchemeSmobVH.cc.i"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/guile/SchemeSmobVH.cc > CMakeFiles/smob.dir/SchemeSmobVH.cc.i

opencog/guile/CMakeFiles/smob.dir/SchemeSmobVH.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/smob.dir/SchemeSmobVH.cc.s"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/guile/SchemeSmobVH.cc -o CMakeFiles/smob.dir/SchemeSmobVH.cc.s

opencog/guile/CMakeFiles/smob.dir/SchemeSmobVH.cc.o.requires:
.PHONY : opencog/guile/CMakeFiles/smob.dir/SchemeSmobVH.cc.o.requires

opencog/guile/CMakeFiles/smob.dir/SchemeSmobVH.cc.o.provides: opencog/guile/CMakeFiles/smob.dir/SchemeSmobVH.cc.o.requires
	$(MAKE) -f opencog/guile/CMakeFiles/smob.dir/build.make opencog/guile/CMakeFiles/smob.dir/SchemeSmobVH.cc.o.provides.build
.PHONY : opencog/guile/CMakeFiles/smob.dir/SchemeSmobVH.cc.o.provides

opencog/guile/CMakeFiles/smob.dir/SchemeSmobVH.cc.o.provides.build: opencog/guile/CMakeFiles/smob.dir/SchemeSmobVH.cc.o

opencog/guile/CMakeFiles/smob.dir/load-file.cc.o: opencog/guile/CMakeFiles/smob.dir/flags.make
opencog/guile/CMakeFiles/smob.dir/load-file.cc.o: ../opencog/guile/load-file.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_11)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/guile/CMakeFiles/smob.dir/load-file.cc.o"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/smob.dir/load-file.cc.o -c /opt/opencog/opencog/guile/load-file.cc

opencog/guile/CMakeFiles/smob.dir/load-file.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/smob.dir/load-file.cc.i"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/guile/load-file.cc > CMakeFiles/smob.dir/load-file.cc.i

opencog/guile/CMakeFiles/smob.dir/load-file.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/smob.dir/load-file.cc.s"
	cd /opt/opencog/bin/opencog/guile && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/guile/load-file.cc -o CMakeFiles/smob.dir/load-file.cc.s

opencog/guile/CMakeFiles/smob.dir/load-file.cc.o.requires:
.PHONY : opencog/guile/CMakeFiles/smob.dir/load-file.cc.o.requires

opencog/guile/CMakeFiles/smob.dir/load-file.cc.o.provides: opencog/guile/CMakeFiles/smob.dir/load-file.cc.o.requires
	$(MAKE) -f opencog/guile/CMakeFiles/smob.dir/build.make opencog/guile/CMakeFiles/smob.dir/load-file.cc.o.provides.build
.PHONY : opencog/guile/CMakeFiles/smob.dir/load-file.cc.o.provides

opencog/guile/CMakeFiles/smob.dir/load-file.cc.o.provides.build: opencog/guile/CMakeFiles/smob.dir/load-file.cc.o

# Object files for target smob
smob_OBJECTS = \
"CMakeFiles/smob.dir/SchemeEval.cc.o" \
"CMakeFiles/smob.dir/SchemeExec.cc.o" \
"CMakeFiles/smob.dir/SchemePrimitive.cc.o" \
"CMakeFiles/smob.dir/SchemeSmob.cc.o" \
"CMakeFiles/smob.dir/SchemeSmobAtom.cc.o" \
"CMakeFiles/smob.dir/SchemeSmobAV.cc.o" \
"CMakeFiles/smob.dir/SchemeSmobGC.cc.o" \
"CMakeFiles/smob.dir/SchemeSmobNew.cc.o" \
"CMakeFiles/smob.dir/SchemeSmobTV.cc.o" \
"CMakeFiles/smob.dir/SchemeSmobVH.cc.o" \
"CMakeFiles/smob.dir/load-file.cc.o"

# External object files for target smob
smob_EXTERNAL_OBJECTS =

opencog/guile/libsmob.so: opencog/guile/CMakeFiles/smob.dir/SchemeEval.cc.o
opencog/guile/libsmob.so: opencog/guile/CMakeFiles/smob.dir/SchemeExec.cc.o
opencog/guile/libsmob.so: opencog/guile/CMakeFiles/smob.dir/SchemePrimitive.cc.o
opencog/guile/libsmob.so: opencog/guile/CMakeFiles/smob.dir/SchemeSmob.cc.o
opencog/guile/libsmob.so: opencog/guile/CMakeFiles/smob.dir/SchemeSmobAtom.cc.o
opencog/guile/libsmob.so: opencog/guile/CMakeFiles/smob.dir/SchemeSmobAV.cc.o
opencog/guile/libsmob.so: opencog/guile/CMakeFiles/smob.dir/SchemeSmobGC.cc.o
opencog/guile/libsmob.so: opencog/guile/CMakeFiles/smob.dir/SchemeSmobNew.cc.o
opencog/guile/libsmob.so: opencog/guile/CMakeFiles/smob.dir/SchemeSmobTV.cc.o
opencog/guile/libsmob.so: opencog/guile/CMakeFiles/smob.dir/SchemeSmobVH.cc.o
opencog/guile/libsmob.so: opencog/guile/CMakeFiles/smob.dir/load-file.cc.o
opencog/guile/libsmob.so: /usr/lib/libguile.so
opencog/guile/libsmob.so: opencog/guile/CMakeFiles/smob.dir/build.make
opencog/guile/libsmob.so: opencog/guile/CMakeFiles/smob.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX shared library libsmob.so"
	cd /opt/opencog/bin/opencog/guile && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/smob.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
opencog/guile/CMakeFiles/smob.dir/build: opencog/guile/libsmob.so
.PHONY : opencog/guile/CMakeFiles/smob.dir/build

opencog/guile/CMakeFiles/smob.dir/requires: opencog/guile/CMakeFiles/smob.dir/SchemeEval.cc.o.requires
opencog/guile/CMakeFiles/smob.dir/requires: opencog/guile/CMakeFiles/smob.dir/SchemeExec.cc.o.requires
opencog/guile/CMakeFiles/smob.dir/requires: opencog/guile/CMakeFiles/smob.dir/SchemePrimitive.cc.o.requires
opencog/guile/CMakeFiles/smob.dir/requires: opencog/guile/CMakeFiles/smob.dir/SchemeSmob.cc.o.requires
opencog/guile/CMakeFiles/smob.dir/requires: opencog/guile/CMakeFiles/smob.dir/SchemeSmobAtom.cc.o.requires
opencog/guile/CMakeFiles/smob.dir/requires: opencog/guile/CMakeFiles/smob.dir/SchemeSmobAV.cc.o.requires
opencog/guile/CMakeFiles/smob.dir/requires: opencog/guile/CMakeFiles/smob.dir/SchemeSmobGC.cc.o.requires
opencog/guile/CMakeFiles/smob.dir/requires: opencog/guile/CMakeFiles/smob.dir/SchemeSmobNew.cc.o.requires
opencog/guile/CMakeFiles/smob.dir/requires: opencog/guile/CMakeFiles/smob.dir/SchemeSmobTV.cc.o.requires
opencog/guile/CMakeFiles/smob.dir/requires: opencog/guile/CMakeFiles/smob.dir/SchemeSmobVH.cc.o.requires
opencog/guile/CMakeFiles/smob.dir/requires: opencog/guile/CMakeFiles/smob.dir/load-file.cc.o.requires
.PHONY : opencog/guile/CMakeFiles/smob.dir/requires

opencog/guile/CMakeFiles/smob.dir/clean:
	cd /opt/opencog/bin/opencog/guile && $(CMAKE_COMMAND) -P CMakeFiles/smob.dir/cmake_clean.cmake
.PHONY : opencog/guile/CMakeFiles/smob.dir/clean

opencog/guile/CMakeFiles/smob.dir/depend:
	cd /opt/opencog/bin && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /opt/opencog /opt/opencog/opencog/guile /opt/opencog/bin /opt/opencog/bin/opencog/guile /opt/opencog/bin/opencog/guile/CMakeFiles/smob.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : opencog/guile/CMakeFiles/smob.dir/depend

