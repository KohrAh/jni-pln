# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canoncical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /opt/opencog

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /opt/opencog/bin

# Include any dependencies generated for this target.
include opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/depend.make

# Include the progress variables for this target.
include opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/progress.make

# Include the compile flags for this target's objects.
include opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/flags.make

opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/MockOpcHCTestExec.cc.o: opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/flags.make
opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/MockOpcHCTestExec.cc.o: ../opencog/embodiment/Control/OperationalAvatarController/MockOpcHCTestExec.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/MockOpcHCTestExec.cc.o"
	cd /opt/opencog/bin/opencog/embodiment/Control/OperationalAvatarController && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/opcHcTester.dir/MockOpcHCTestExec.cc.o -c /opt/opencog/opencog/embodiment/Control/OperationalAvatarController/MockOpcHCTestExec.cc

opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/MockOpcHCTestExec.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/opcHcTester.dir/MockOpcHCTestExec.cc.i"
	cd /opt/opencog/bin/opencog/embodiment/Control/OperationalAvatarController && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/embodiment/Control/OperationalAvatarController/MockOpcHCTestExec.cc > CMakeFiles/opcHcTester.dir/MockOpcHCTestExec.cc.i

opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/MockOpcHCTestExec.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/opcHcTester.dir/MockOpcHCTestExec.cc.s"
	cd /opt/opencog/bin/opencog/embodiment/Control/OperationalAvatarController && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/embodiment/Control/OperationalAvatarController/MockOpcHCTestExec.cc -o CMakeFiles/opcHcTester.dir/MockOpcHCTestExec.cc.s

opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/MockOpcHCTestExec.cc.o.requires:
.PHONY : opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/MockOpcHCTestExec.cc.o.requires

opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/MockOpcHCTestExec.cc.o.provides: opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/MockOpcHCTestExec.cc.o.requires
	$(MAKE) -f opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/build.make opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/MockOpcHCTestExec.cc.o.provides.build
.PHONY : opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/MockOpcHCTestExec.cc.o.provides

opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/MockOpcHCTestExec.cc.o.provides.build: opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/MockOpcHCTestExec.cc.o

# Object files for target opcHcTester
opcHcTester_OBJECTS = \
"CMakeFiles/opcHcTester.dir/MockOpcHCTestExec.cc.o"

# External object files for target opcHcTester
opcHcTester_EXTERNAL_OBJECTS =

opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/MockOpcHCTestExec.cc.o
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/Control/OperationalAvatarController/liboac.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/comboreduct/libcomboreduct.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/AvatarComboVocabulary/libAvatarComboVocabulary.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/Learning/NoSpaceLife/libImaginaryLife.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/Control/OperationalAvatarController/liboac.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/Control/PerceptionActionInterface/libPAI.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/Learning/behavior/libbehavior.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/Control/Procedure/libProcedure.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/WorldWrapper/libWorldWrapper.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/Control/OperationalAvatarController/liboac.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/Control/PerceptionActionInterface/libPAI.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/Learning/behavior/libbehavior.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/Control/Procedure/libProcedure.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/WorldWrapper/libWorldWrapper.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/web/json_spirit/libjson_spirit.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/Control/PredicateUpdaters/libPredicateUpdaters.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/persist/file/libsavable.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/dynamics/attention/libattention.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/cython/libPythonModule.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/cython/libPythonEval.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/cython/logic_wrapper.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/reasoning/pln/libpln.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/ubigraph/libubigraph.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/i386-linux-gnu/libcurl.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/libxmlrpc_client.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/libxmlrpc.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/libxmlrpc_util.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/libxmlrpc_xmlparse.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/libxmlrpc_xmltok.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/i386-linux-gnu/libcurl.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/libxmlrpc_client.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/libxmlrpc.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/libxmlrpc_util.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/libxmlrpc_xmlparse.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/libxmlrpc_xmltok.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/libpython2.7.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/libprotobuf.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/Control/MessagingSystem/libMessagingSystem.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/Learning/LearningServerMessages/libLearningServerMessages.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/Control/MessagingSystem/libMessagingSystem.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/Learning/LearningServerMessages/libLearningServerMessages.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/libboost_date_time-mt.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/Control/libControl.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/AvatarComboVocabulary/libAvatarComboVocabulary.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/comboreduct/libcomboreduct.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/spatial/libTangentBug.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/spatial/libAStar.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/spatial/libHPASearch.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/RuleValidation/VirtualWorldData/libVirtualWorldData.a
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/libxerces-c.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/AtomSpaceExtensions/libAtomSpaceExtensions.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/query/libquery.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/server/libserver.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/guile/libsmob.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/libguile.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/atomspace/libatomspace.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/util/libutil.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/libboost_system-mt.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/libboost_regex-mt.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/libboost_signals-mt.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/spatial/libSpaceMap.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/persist/xml/libxml.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/i386-linux-gnu/libexpat.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/libboost_filesystem-mt.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/libboost_thread-mt.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/persist/sql/libpersist.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: /usr/lib/libodbc.so
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/build.make
opencog/embodiment/Control/OperationalAvatarController/opcHcTester: opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable opcHcTester"
	cd /opt/opencog/bin/opencog/embodiment/Control/OperationalAvatarController && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/opcHcTester.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/build: opencog/embodiment/Control/OperationalAvatarController/opcHcTester
.PHONY : opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/build

opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/requires: opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/MockOpcHCTestExec.cc.o.requires
.PHONY : opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/requires

opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/clean:
	cd /opt/opencog/bin/opencog/embodiment/Control/OperationalAvatarController && $(CMAKE_COMMAND) -P CMakeFiles/opcHcTester.dir/cmake_clean.cmake
.PHONY : opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/clean

opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/depend:
	cd /opt/opencog/bin && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /opt/opencog /opt/opencog/opencog/embodiment/Control/OperationalAvatarController /opt/opencog/bin /opt/opencog/bin/opencog/embodiment/Control/OperationalAvatarController /opt/opencog/bin/opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/opcHcTester.dir/depend

