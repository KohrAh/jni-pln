# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/embodiment/Control/OperationalAvatarController/OACExecutable.cc" "/opt/opencog/bin/opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/oac-exec.dir/OACExecutable.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opt/opencog/bin/opencog/embodiment/Control/OperationalAvatarController/CMakeFiles/oac.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/embodiment/AvatarComboVocabulary/CMakeFiles/AvatarComboVocabulary.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/embodiment/Learning/NoSpaceLife/CMakeFiles/ImaginaryLife.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/embodiment/Control/PerceptionActionInterface/CMakeFiles/PAI.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/embodiment/Learning/behavior/CMakeFiles/behavior.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/embodiment/Control/Procedure/CMakeFiles/Procedure.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/embodiment/WorldWrapper/CMakeFiles/WorldWrapper.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/web/json_spirit/CMakeFiles/json_spirit.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/embodiment/Control/PredicateUpdaters/CMakeFiles/PredicateUpdaters.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/persist/file/CMakeFiles/savable.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/dynamics/attention/CMakeFiles/attention.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/cython/CMakeFiles/PythonModule.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/cython/CMakeFiles/PythonEval.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/cython/CMakeFiles/logic_wrapper.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/ubigraph/CMakeFiles/ubigraph.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/embodiment/Control/MessagingSystem/CMakeFiles/MessagingSystem.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/embodiment/Learning/LearningServerMessages/CMakeFiles/LearningServerMessages.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/embodiment/Control/CMakeFiles/Control.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/spatial/CMakeFiles/TangentBug.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/spatial/CMakeFiles/AStar.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/spatial/CMakeFiles/HPASearch.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/embodiment/RuleValidation/VirtualWorldData/CMakeFiles/VirtualWorldData.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/embodiment/AtomSpaceExtensions/CMakeFiles/AtomSpaceExtensions.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/query/CMakeFiles/query.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/server/CMakeFiles/server.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/guile/CMakeFiles/smob.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/persist/xml/CMakeFiles/xml.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/persist/sql/CMakeFiles/persist.dir/DependInfo.cmake"
  )
