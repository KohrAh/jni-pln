# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/embodiment/Control/MessagingSystem/EmbodimentCogServer.cc" "/opt/opencog/bin/opencog/embodiment/Control/MessagingSystem/CMakeFiles/MessagingSystem.dir/EmbodimentCogServer.cc.o"
  "/opt/opencog/opencog/embodiment/Control/MessagingSystem/FeedbackMessage.cc" "/opt/opencog/bin/opencog/embodiment/Control/MessagingSystem/CMakeFiles/MessagingSystem.dir/FeedbackMessage.cc.o"
  "/opt/opencog/opencog/embodiment/Control/MessagingSystem/FileMessageCentral.cc" "/opt/opencog/bin/opencog/embodiment/Control/MessagingSystem/CMakeFiles/MessagingSystem.dir/FileMessageCentral.cc.o"
  "/opt/opencog/opencog/embodiment/Control/MessagingSystem/MemoryMessageCentral.cc" "/opt/opencog/bin/opencog/embodiment/Control/MessagingSystem/CMakeFiles/MessagingSystem.dir/MemoryMessageCentral.cc.o"
  "/opt/opencog/opencog/embodiment/Control/MessagingSystem/Message.cc" "/opt/opencog/bin/opencog/embodiment/Control/MessagingSystem/CMakeFiles/MessagingSystem.dir/Message.cc.o"
  "/opt/opencog/opencog/embodiment/Control/MessagingSystem/MessageCentral.cc" "/opt/opencog/bin/opencog/embodiment/Control/MessagingSystem/CMakeFiles/MessagingSystem.dir/MessageCentral.cc.o"
  "/opt/opencog/opencog/embodiment/Control/MessagingSystem/MessagingSystemExceptions.cc" "/opt/opencog/bin/opencog/embodiment/Control/MessagingSystem/CMakeFiles/MessagingSystem.dir/MessagingSystemExceptions.cc.o"
  "/opt/opencog/opencog/embodiment/Control/MessagingSystem/NetworkElement.cc" "/opt/opencog/bin/opencog/embodiment/Control/MessagingSystem/CMakeFiles/MessagingSystem.dir/NetworkElement.cc.o"
  "/opt/opencog/opencog/embodiment/Control/MessagingSystem/NetworkElementCommon.cc" "/opt/opencog/bin/opencog/embodiment/Control/MessagingSystem/CMakeFiles/MessagingSystem.dir/NetworkElementCommon.cc.o"
  "/opt/opencog/opencog/embodiment/Control/MessagingSystem/RawMessage.cc" "/opt/opencog/bin/opencog/embodiment/Control/MessagingSystem/CMakeFiles/MessagingSystem.dir/RawMessage.cc.o"
  "/opt/opencog/opencog/embodiment/Control/MessagingSystem/Router.cc" "/opt/opencog/bin/opencog/embodiment/Control/MessagingSystem/CMakeFiles/MessagingSystem.dir/Router.cc.o"
  "/opt/opencog/opencog/embodiment/Control/MessagingSystem/RouterMessage.cc" "/opt/opencog/bin/opencog/embodiment/Control/MessagingSystem/CMakeFiles/MessagingSystem.dir/RouterMessage.cc.o"
  "/opt/opencog/opencog/embodiment/Control/MessagingSystem/RouterServerSocket.cc" "/opt/opencog/bin/opencog/embodiment/Control/MessagingSystem/CMakeFiles/MessagingSystem.dir/RouterServerSocket.cc.o"
  "/opt/opencog/opencog/embodiment/Control/MessagingSystem/ServerSocket.cc" "/opt/opencog/bin/opencog/embodiment/Control/MessagingSystem/CMakeFiles/MessagingSystem.dir/ServerSocket.cc.o"
  "/opt/opencog/opencog/embodiment/Control/MessagingSystem/Spawner.cc" "/opt/opencog/bin/opencog/embodiment/Control/MessagingSystem/CMakeFiles/MessagingSystem.dir/Spawner.cc.o"
  "/opt/opencog/opencog/embodiment/Control/MessagingSystem/StringMessage.cc" "/opt/opencog/bin/opencog/embodiment/Control/MessagingSystem/CMakeFiles/MessagingSystem.dir/StringMessage.cc.o"
  "/opt/opencog/opencog/embodiment/Control/MessagingSystem/TickMessage.cc" "/opt/opencog/bin/opencog/embodiment/Control/MessagingSystem/CMakeFiles/MessagingSystem.dir/TickMessage.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  "_REENTRANT"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opt/opencog/bin/opencog/embodiment/Learning/LearningServerMessages/CMakeFiles/LearningServerMessages.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/embodiment/Control/CMakeFiles/Control.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/persist/xml/CMakeFiles/xml.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/server/CMakeFiles/server.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/embodiment/AvatarComboVocabulary/CMakeFiles/AvatarComboVocabulary.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/embodiment/AtomSpaceExtensions/CMakeFiles/AtomSpaceExtensions.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/query/CMakeFiles/query.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/persist/sql/CMakeFiles/persist.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/guile/CMakeFiles/smob.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/DependInfo.cmake"
  )
