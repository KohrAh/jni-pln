# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/embodiment/Control/PredicateUpdaters/BasicPredicateUpdater.cc" "/opt/opencog/bin/opencog/embodiment/Control/PredicateUpdaters/CMakeFiles/PredicateUpdaters.dir/BasicPredicateUpdater.cc.o"
  "/opt/opencog/opencog/embodiment/Control/PredicateUpdaters/IsMovablePredicateUpdater.cc" "/opt/opencog/bin/opencog/embodiment/Control/PredicateUpdaters/CMakeFiles/PredicateUpdaters.dir/IsMovablePredicateUpdater.cc.o"
  "/opt/opencog/opencog/embodiment/Control/PredicateUpdaters/IsNoisyPredicateUpdater.cc" "/opt/opencog/bin/opencog/embodiment/Control/PredicateUpdaters/CMakeFiles/PredicateUpdaters.dir/IsNoisyPredicateUpdater.cc.o"
  "/opt/opencog/opencog/embodiment/Control/PredicateUpdaters/IsPeePlacePredicateUpdater.cc" "/opt/opencog/bin/opencog/embodiment/Control/PredicateUpdaters/CMakeFiles/PredicateUpdaters.dir/IsPeePlacePredicateUpdater.cc.o"
  "/opt/opencog/opencog/embodiment/Control/PredicateUpdaters/IsPickupablePredicateUpdater.cc" "/opt/opencog/bin/opencog/embodiment/Control/PredicateUpdaters/CMakeFiles/PredicateUpdaters.dir/IsPickupablePredicateUpdater.cc.o"
  "/opt/opencog/opencog/embodiment/Control/PredicateUpdaters/IsPooPlacePredicateUpdater.cc" "/opt/opencog/bin/opencog/embodiment/Control/PredicateUpdaters/CMakeFiles/PredicateUpdaters.dir/IsPooPlacePredicateUpdater.cc.o"
  "/opt/opencog/opencog/embodiment/Control/PredicateUpdaters/IsSmallPredicateUpdater.cc" "/opt/opencog/bin/opencog/embodiment/Control/PredicateUpdaters/CMakeFiles/PredicateUpdaters.dir/IsSmallPredicateUpdater.cc.o"
  "/opt/opencog/opencog/embodiment/Control/PredicateUpdaters/PetPsychePredicatesUpdater.cc" "/opt/opencog/bin/opencog/embodiment/Control/PredicateUpdaters/CMakeFiles/PredicateUpdaters.dir/PetPsychePredicatesUpdater.cc.o"
  "/opt/opencog/opencog/embodiment/Control/PredicateUpdaters/PredicatesUpdater.cc" "/opt/opencog/bin/opencog/embodiment/Control/PredicateUpdaters/CMakeFiles/PredicateUpdaters.dir/PredicatesUpdater.cc.o"
  "/opt/opencog/opencog/embodiment/Control/PredicateUpdaters/SpatialPredicateUpdater.cc" "/opt/opencog/bin/opencog/embodiment/Control/PredicateUpdaters/CMakeFiles/PredicateUpdaters.dir/SpatialPredicateUpdater.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opt/opencog/bin/opencog/embodiment/AtomSpaceExtensions/CMakeFiles/AtomSpaceExtensions.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/query/CMakeFiles/query.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/server/CMakeFiles/server.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/persist/xml/CMakeFiles/xml.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/guile/CMakeFiles/smob.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/persist/sql/CMakeFiles/persist.dir/DependInfo.cmake"
  )
