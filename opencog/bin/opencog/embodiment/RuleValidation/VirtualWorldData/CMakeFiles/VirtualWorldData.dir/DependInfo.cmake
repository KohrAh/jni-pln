# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/embodiment/RuleValidation/VirtualWorldData/VirtualEntity.cc" "/opt/opencog/bin/opencog/embodiment/RuleValidation/VirtualWorldData/CMakeFiles/VirtualWorldData.dir/VirtualEntity.cc.o"
  "/opt/opencog/opencog/embodiment/RuleValidation/VirtualWorldData/VirtualWorldActions.cc" "/opt/opencog/bin/opencog/embodiment/RuleValidation/VirtualWorldData/CMakeFiles/VirtualWorldData.dir/VirtualWorldActions.cc.o"
  "/opt/opencog/opencog/embodiment/RuleValidation/VirtualWorldData/VirtualWorldState.cc" "/opt/opencog/bin/opencog/embodiment/RuleValidation/VirtualWorldData/CMakeFiles/VirtualWorldData.dir/VirtualWorldState.cc.o"
  "/opt/opencog/opencog/embodiment/RuleValidation/VirtualWorldData/XmlErrorHandler.cc" "/opt/opencog/bin/opencog/embodiment/RuleValidation/VirtualWorldData/CMakeFiles/VirtualWorldData.dir/XmlErrorHandler.cc.o"
  "/opt/opencog/opencog/embodiment/RuleValidation/VirtualWorldData/XmlLoader.cc" "/opt/opencog/bin/opencog/embodiment/RuleValidation/VirtualWorldData/CMakeFiles/VirtualWorldData.dir/XmlLoader.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/DependInfo.cmake"
  )
