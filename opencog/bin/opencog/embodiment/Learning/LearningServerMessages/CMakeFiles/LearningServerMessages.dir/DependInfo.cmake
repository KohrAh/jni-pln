# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/embodiment/Learning/LearningServerMessages/LSCmdMessage.cc" "/opt/opencog/bin/opencog/embodiment/Learning/LearningServerMessages/CMakeFiles/LearningServerMessages.dir/LSCmdMessage.cc.o"
  "/opt/opencog/opencog/embodiment/Learning/LearningServerMessages/LearnMessage.cc" "/opt/opencog/bin/opencog/embodiment/Learning/LearningServerMessages/CMakeFiles/LearningServerMessages.dir/LearnMessage.cc.o"
  "/opt/opencog/opencog/embodiment/Learning/LearningServerMessages/RewardMessage.cc" "/opt/opencog/bin/opencog/embodiment/Learning/LearningServerMessages/CMakeFiles/LearningServerMessages.dir/RewardMessage.cc.o"
  "/opt/opencog/opencog/embodiment/Learning/LearningServerMessages/SchemaMessage.cc" "/opt/opencog/bin/opencog/embodiment/Learning/LearningServerMessages/CMakeFiles/LearningServerMessages.dir/SchemaMessage.cc.o"
  "/opt/opencog/opencog/embodiment/Learning/LearningServerMessages/StopLearningMessage.cc" "/opt/opencog/bin/opencog/embodiment/Learning/LearningServerMessages/CMakeFiles/LearningServerMessages.dir/StopLearningMessage.cc.o"
  "/opt/opencog/opencog/embodiment/Learning/LearningServerMessages/TrySchemaMessage.cc" "/opt/opencog/bin/opencog/embodiment/Learning/LearningServerMessages/CMakeFiles/LearningServerMessages.dir/TrySchemaMessage.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opt/opencog/bin/opencog/embodiment/Control/MessagingSystem/CMakeFiles/MessagingSystem.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/embodiment/AvatarComboVocabulary/CMakeFiles/AvatarComboVocabulary.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/persist/xml/CMakeFiles/xml.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/embodiment/AtomSpaceExtensions/CMakeFiles/AtomSpaceExtensions.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/embodiment/Control/CMakeFiles/Control.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/query/CMakeFiles/query.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/server/CMakeFiles/server.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/persist/sql/CMakeFiles/persist.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/guile/CMakeFiles/smob.dir/DependInfo.cmake"
  )
