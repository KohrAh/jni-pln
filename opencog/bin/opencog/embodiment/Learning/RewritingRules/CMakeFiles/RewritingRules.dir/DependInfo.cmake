# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/embodiment/Learning/RewritingRules/hillclimbing_action_reduction.cc" "/opt/opencog/bin/opencog/embodiment/Learning/RewritingRules/CMakeFiles/RewritingRules.dir/hillclimbing_action_reduction.cc.o"
  "/opt/opencog/opencog/embodiment/Learning/RewritingRules/hillclimbing_full_reduction.cc" "/opt/opencog/bin/opencog/embodiment/Learning/RewritingRules/CMakeFiles/RewritingRules.dir/hillclimbing_full_reduction.cc.o"
  "/opt/opencog/opencog/embodiment/Learning/RewritingRules/hillclimbing_perception_reduction.cc" "/opt/opencog/bin/opencog/embodiment/Learning/RewritingRules/CMakeFiles/RewritingRules.dir/hillclimbing_perception_reduction.cc.o"
  "/opt/opencog/opencog/embodiment/Learning/RewritingRules/post_learning_rewriting.cc" "/opt/opencog/bin/opencog/embodiment/Learning/RewritingRules/CMakeFiles/RewritingRules.dir/post_learning_rewriting.cc.o"
  "/opt/opencog/opencog/embodiment/Learning/RewritingRules/post_learning_rules.cc" "/opt/opencog/bin/opencog/embodiment/Learning/RewritingRules/CMakeFiles/RewritingRules.dir/post_learning_rules.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/embodiment/AvatarComboVocabulary/CMakeFiles/AvatarComboVocabulary.dir/DependInfo.cmake"
  )
