# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/learning/moses/example-progs/moses-ann-pole2-hillclimbing.cc" "/opt/opencog/bin/opencog/learning/moses/example-progs/CMakeFiles/moses-ann-pole2-hillclimbing.dir/moses-ann-pole2-hillclimbing.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opt/opencog/bin/opencog/learning/moses/CMakeFiles/moses.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/DependInfo.cmake"
  )
