# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/learning/moses/main/moses_exec.cc" "/opt/opencog/bin/opencog/learning/moses/main/CMakeFiles/moses_exec.dir/moses_exec.cc.o"
  "/opt/opencog/opencog/learning/moses/main/moses_exec_def.cc" "/opt/opencog/bin/opencog/learning/moses/main/CMakeFiles/moses_exec.dir/moses_exec_def.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )
