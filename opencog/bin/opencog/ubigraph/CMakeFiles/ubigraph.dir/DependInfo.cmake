# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/opt/opencog/opencog/ubigraph/client.c" "/opt/opencog/bin/opencog/ubigraph/CMakeFiles/ubigraph.dir/client.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/ubigraph/BITUbigrapher.cc" "/opt/opencog/bin/opencog/ubigraph/CMakeFiles/ubigraph.dir/BITUbigrapher.cc.o"
  "/opt/opencog/opencog/ubigraph/UbigraphModule.cc" "/opt/opencog/bin/opencog/ubigraph/CMakeFiles/ubigraph.dir/UbigraphModule.cc.o"
  "/opt/opencog/opencog/ubigraph/Ubigrapher.cc" "/opt/opencog/bin/opencog/ubigraph/CMakeFiles/ubigraph.dir/Ubigrapher.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )
