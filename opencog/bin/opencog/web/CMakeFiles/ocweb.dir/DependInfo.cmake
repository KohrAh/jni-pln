# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/opt/opencog/opencog/web/mongoose.c" "/opt/opencog/bin/opencog/web/CMakeFiles/ocweb.dir/mongoose.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/web/AtomURLHandler.cc" "/opt/opencog/bin/opencog/web/CMakeFiles/ocweb.dir/AtomURLHandler.cc.o"
  "/opt/opencog/opencog/web/BaseURLHandler.cc" "/opt/opencog/bin/opencog/web/CMakeFiles/ocweb.dir/BaseURLHandler.cc.o"
  "/opt/opencog/opencog/web/CreateAtomRequest.cc" "/opt/opencog/bin/opencog/web/CMakeFiles/ocweb.dir/CreateAtomRequest.cc.o"
  "/opt/opencog/opencog/web/GetAtomRequest.cc" "/opt/opencog/bin/opencog/web/CMakeFiles/ocweb.dir/GetAtomRequest.cc.o"
  "/opt/opencog/opencog/web/GetListRequest.cc" "/opt/opencog/bin/opencog/web/CMakeFiles/ocweb.dir/GetListRequest.cc.o"
  "/opt/opencog/opencog/web/JsonUtil.cc" "/opt/opencog/bin/opencog/web/CMakeFiles/ocweb.dir/JsonUtil.cc.o"
  "/opt/opencog/opencog/web/ListURLHandler.cc" "/opt/opencog/bin/opencog/web/CMakeFiles/ocweb.dir/ListURLHandler.cc.o"
  "/opt/opencog/opencog/web/ServerRequestWrapper.cc" "/opt/opencog/bin/opencog/web/CMakeFiles/ocweb.dir/ServerRequestWrapper.cc.o"
  "/opt/opencog/opencog/web/UpdateAtomRequest.cc" "/opt/opencog/bin/opencog/web/CMakeFiles/ocweb.dir/UpdateAtomRequest.cc.o"
  "/opt/opencog/opencog/web/WebModule.cc" "/opt/opencog/bin/opencog/web/CMakeFiles/ocweb.dir/WebModule.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/server/CMakeFiles/server.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/web/json_spirit/CMakeFiles/json_spirit.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/persist/sql/CMakeFiles/persist.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/guile/CMakeFiles/smob.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/persist/xml/CMakeFiles/xml.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/DependInfo.cmake"
  )
