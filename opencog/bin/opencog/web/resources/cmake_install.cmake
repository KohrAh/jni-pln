# Install script for directory: /opt/opencog/opencog/web/resources

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Debug")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CPACK_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/opencog/www/resources/ab.css;/usr/local/share/opencog/www/resources/init.js;/usr/local/share/opencog/www/resources/processing.js;/usr/local/share/opencog/www/resources/local_graph.js;/usr/local/share/opencog/www/resources/head.html;/usr/local/share/opencog/www/resources/intro.html;/usr/local/share/opencog/www/resources/viewatom.html;/usr/local/share/opencog/www/resources/addatom.html;/usr/local/share/opencog/www/resources/update.html;/usr/local/share/opencog/www/resources/updatett.html;/usr/local/share/opencog/www/resources/updatesti.html;/usr/local/share/opencog/www/resources/updatelti.html;/usr/local/share/opencog/www/resources/Logo.png")
FILE(INSTALL DESTINATION "/usr/local/share/opencog/www/resources" TYPE FILE FILES
    "/opt/opencog/opencog/web/resources/ab.css"
    "/opt/opencog/opencog/web/resources/init.js"
    "/opt/opencog/opencog/web/resources/processing.js"
    "/opt/opencog/opencog/web/resources/local_graph.js"
    "/opt/opencog/opencog/web/resources/head.html"
    "/opt/opencog/opencog/web/resources/intro.html"
    "/opt/opencog/opencog/web/resources/viewatom.html"
    "/opt/opencog/opencog/web/resources/addatom.html"
    "/opt/opencog/opencog/web/resources/update.html"
    "/opt/opencog/opencog/web/resources/updatett.html"
    "/opt/opencog/opencog/web/resources/updatesti.html"
    "/opt/opencog/opencog/web/resources/updatelti.html"
    "/opt/opencog/opencog/web/resources/Logo.png"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

