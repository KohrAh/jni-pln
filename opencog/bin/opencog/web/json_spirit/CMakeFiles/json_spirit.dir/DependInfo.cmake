# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/web/json_spirit/json_spirit_reader.cpp" "/opt/opencog/bin/opencog/web/json_spirit/CMakeFiles/json_spirit.dir/json_spirit_reader.cpp.o"
  "/opt/opencog/opencog/web/json_spirit/json_spirit_value.cpp" "/opt/opencog/bin/opencog/web/json_spirit/CMakeFiles/json_spirit.dir/json_spirit_value.cpp.o"
  "/opt/opencog/opencog/web/json_spirit/json_spirit_writer.cpp" "/opt/opencog/bin/opencog/web/json_spirit/CMakeFiles/json_spirit.dir/json_spirit_writer.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )
