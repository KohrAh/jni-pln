FILE(REMOVE_RECURSE
  "CMakeFiles/attention.dir/AttentionModule.cc.o"
  "CMakeFiles/attention.dir/ForgettingAgent.cc.o"
  "CMakeFiles/attention.dir/HebbianUpdatingAgent.cc.o"
  "CMakeFiles/attention.dir/ImportanceSpreadingAgent.cc.o"
  "CMakeFiles/attention.dir/ImportanceDiffusionAgent.cc.o"
  "CMakeFiles/attention.dir/ImportanceUpdatingAgent.cc.o"
  "CMakeFiles/attention.dir/STIDecayingAgent.cc.o"
  "../../../../opencog/dynamics/attention/atom_types.h"
  "../../../../opencog/dynamics/attention/atom_types.definitions"
  "../../../../opencog/dynamics/attention/atom_types.inheritance"
  "libattention.pdb"
  "libattention.so"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang CXX)
  INCLUDE(CMakeFiles/attention.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
