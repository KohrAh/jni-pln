# Install script for directory: /opt/opencog/opencog

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Debug")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("/opt/opencog/bin/opencog/util/cmake_install.cmake")
  INCLUDE("/opt/opencog/bin/opencog/atomspace/cmake_install.cmake")
  INCLUDE("/opt/opencog/bin/opencog/benchmark/cmake_install.cmake")
  INCLUDE("/opt/opencog/bin/opencog/query/cmake_install.cmake")
  INCLUDE("/opt/opencog/bin/opencog/nlp/cmake_install.cmake")
  INCLUDE("/opt/opencog/bin/opencog/dynamics/cmake_install.cmake")
  INCLUDE("/opt/opencog/bin/opencog/server/cmake_install.cmake")
  INCLUDE("/opt/opencog/bin/opencog/adaptors/cmake_install.cmake")
  INCLUDE("/opt/opencog/bin/opencog/web/cmake_install.cmake")
  INCLUDE("/opt/opencog/bin/opencog/guile/cmake_install.cmake")
  INCLUDE("/opt/opencog/bin/opencog/scm/cmake_install.cmake")
  INCLUDE("/opt/opencog/bin/opencog/shell/cmake_install.cmake")
  INCLUDE("/opt/opencog/bin/opencog/cython/cmake_install.cmake")
  INCLUDE("/opt/opencog/bin/opencog/persist/cmake_install.cmake")
  INCLUDE("/opt/opencog/bin/opencog/reasoning/cmake_install.cmake")
  INCLUDE("/opt/opencog/bin/opencog/comboreduct/cmake_install.cmake")
  INCLUDE("/opt/opencog/bin/opencog/spatial/cmake_install.cmake")
  INCLUDE("/opt/opencog/bin/opencog/learning/cmake_install.cmake")
  INCLUDE("/opt/opencog/bin/opencog/embodiment/cmake_install.cmake")
  INCLUDE("/opt/opencog/bin/opencog/dotty/cmake_install.cmake")
  INCLUDE("/opt/opencog/bin/opencog/ubigraph/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

