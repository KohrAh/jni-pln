# Install script for directory: /opt/opencog/opencog/cython

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Debug")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libPythonModule.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libPythonModule.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libPythonModule.so"
         RPATH "/usr/local/lib/opencog")
  ENDIF()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/opencog" TYPE SHARED_LIBRARY FILES "/opt/opencog/bin/opencog/cython/libPythonModule.so")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libPythonModule.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libPythonModule.so")
    FILE(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libPythonModule.so"
         OLD_RPATH "/opt/opencog/bin/opencog/server:/opt/opencog/bin/opencog/cython:/opt/opencog/bin/opencog/persist/sql:/opt/opencog/bin/opencog/guile:/opt/opencog/bin/opencog/atomspace:/opt/opencog/bin/opencog/persist/xml:/opt/opencog/bin/opencog/util:/opt/opencog/bin/opencog/spatial:"
         NEW_RPATH "/usr/local/lib/opencog")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libPythonModule.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}/usr/local/share/opencog/python/agent_finder.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/opencog/python/agent_finder.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/opencog/python/agent_finder.so"
         RPATH "/usr/local/lib/opencog")
  ENDIF()
  list(APPEND CPACK_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/opencog/python/agent_finder.so")
FILE(INSTALL DESTINATION "/usr/local/share/opencog/python" TYPE SHARED_LIBRARY FILES "/opt/opencog/bin/opencog/cython/agent_finder.so")
  IF(EXISTS "$ENV{DESTDIR}/usr/local/share/opencog/python/agent_finder.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/opencog/python/agent_finder.so")
    FILE(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/usr/local/share/opencog/python/agent_finder.so"
         OLD_RPATH "::::::::::::::::::::::"
         NEW_RPATH "/usr/local/lib/opencog")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/opencog/python/agent_finder.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}/usr/local/share/opencog/python/opencog/opencog/atomspace.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/opencog/python/opencog/opencog/atomspace.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/opencog/python/opencog/opencog/atomspace.so"
         RPATH "/usr/local/lib/opencog")
  ENDIF()
  list(APPEND CPACK_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/opencog/python/opencog/atomspace.so")
FILE(INSTALL DESTINATION "/usr/local/share/opencog/python/opencog" TYPE SHARED_LIBRARY FILES "/opt/opencog/bin/opencog/cython/opencog/atomspace.so")
  IF(EXISTS "$ENV{DESTDIR}/usr/local/share/opencog/python/opencog/opencog/atomspace.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/opencog/python/opencog/opencog/atomspace.so")
    FILE(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/usr/local/share/opencog/python/opencog/opencog/atomspace.so"
         OLD_RPATH "/opt/opencog/bin/opencog/atomspace:/opt/opencog/bin/opencog/util:/opt/opencog/bin/opencog/persist/xml:/opt/opencog/bin/opencog/spatial:"
         NEW_RPATH "/usr/local/lib/opencog")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/opencog/python/opencog/opencog/atomspace.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}/usr/local/share/opencog/python/opencog/opencog/cogserver.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/opencog/python/opencog/opencog/cogserver.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/opencog/python/opencog/opencog/cogserver.so"
         RPATH "/usr/local/lib/opencog")
  ENDIF()
  list(APPEND CPACK_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/opencog/python/opencog/cogserver.so")
FILE(INSTALL DESTINATION "/usr/local/share/opencog/python/opencog" TYPE SHARED_LIBRARY FILES "/opt/opencog/bin/opencog/cython/opencog/cogserver.so")
  IF(EXISTS "$ENV{DESTDIR}/usr/local/share/opencog/python/opencog/opencog/cogserver.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/opencog/python/opencog/opencog/cogserver.so")
    FILE(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/usr/local/share/opencog/python/opencog/opencog/cogserver.so"
         OLD_RPATH "::::::::::::::::::::::"
         NEW_RPATH "/usr/local/lib/opencog")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/opencog/python/opencog/opencog/cogserver.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CPACK_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/opencog/python/opencog/__init__.py")
FILE(INSTALL DESTINATION "/usr/local/share/opencog/python/opencog" TYPE FILE FILES "/opt/opencog/opencog/cython/opencog/__init__.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

