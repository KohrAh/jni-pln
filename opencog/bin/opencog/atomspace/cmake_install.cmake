# Install script for directory: /opt/opencog/opencog/atomspace

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Debug")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libatomspace.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libatomspace.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libatomspace.so"
         RPATH "/usr/local/lib/opencog")
  ENDIF()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/opencog" TYPE SHARED_LIBRARY FILES "/opt/opencog/bin/opencog/atomspace/libatomspace.so")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libatomspace.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libatomspace.so")
    FILE(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libatomspace.so"
         OLD_RPATH "/opt/opencog/bin/opencog/persist/xml:/opt/opencog/bin/opencog/util:/opt/opencog/bin/opencog/spatial:"
         NEW_RPATH "/usr/local/lib/opencog")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libatomspace.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/opencog/atomspace" TYPE FILE FILES
    "/opt/opencog/opencog/atomspace/ASRequest.h"
    "/opt/opencog/opencog/atomspace/Atom.h"
    "/opt/opencog/opencog/atomspace/AtomIndex.h"
    "/opt/opencog/opencog/atomspace/AtomSpaceDefinitions.h"
    "/opt/opencog/opencog/atomspace/AtomSpace.h"
    "/opt/opencog/opencog/atomspace/AtomSpaceAsync.h"
    "/opt/opencog/opencog/atomspace/AtomSpaceImpl.h"
    "/opt/opencog/opencog/atomspace/AtomTable.h"
    "/opt/opencog/opencog/atomspace/atom_types.h"
    "/opt/opencog/opencog/atomspace/AttentionValue.h"
    "/opt/opencog/opencog/atomspace/AttentionBank.h"
    "/opt/opencog/opencog/atomspace/BackingStore.h"
    "/opt/opencog/opencog/atomspace/ClassServer.h"
    "/opt/opencog/opencog/atomspace/CompositeTruthValue.h"
    "/opt/opencog/opencog/atomspace/CountTruthValue.h"
    "/opt/opencog/opencog/atomspace/FixedIntegerIndex.h"
    "/opt/opencog/opencog/atomspace/FollowLink.h"
    "/opt/opencog/opencog/atomspace/ForeachChaseLink.h"
    "/opt/opencog/opencog/atomspace/Foreach.h"
    "/opt/opencog/opencog/atomspace/ForeachTwo.h"
    "/opt/opencog/opencog/atomspace/Handle.h"
    "/opt/opencog/opencog/atomspace/HandleEntry.h"
    "/opt/opencog/opencog/atomspace/HandleIterator.h"
    "/opt/opencog/opencog/atomspace/HandleMap.h"
    "/opt/opencog/opencog/atomspace/HandleSeqIndex.h"
    "/opt/opencog/opencog/atomspace/HandleSet.h"
    "/opt/opencog/opencog/atomspace/HandleTemporalPairEntry.h"
    "/opt/opencog/opencog/atomspace/HandleTemporalPair.h"
    "/opt/opencog/opencog/atomspace/HandleToTemporalEntryMap.h"
    "/opt/opencog/opencog/atomspace/IndefiniteTruthValue.h"
    "/opt/opencog/opencog/atomspace/ImportanceIndex.h"
    "/opt/opencog/opencog/atomspace/Link.h"
    "/opt/opencog/opencog/atomspace/LinkIndex.h"
    "/opt/opencog/opencog/atomspace/NameIndex.h"
    "/opt/opencog/opencog/atomspace/Node.h"
    "/opt/opencog/opencog/atomspace/NodeIndex.h"
    "/opt/opencog/opencog/atomspace/NullTruthValue.h"
    "/opt/opencog/opencog/atomspace/PredicateEvaluator.h"
    "/opt/opencog/opencog/atomspace/PredicateIndex.h"
    "/opt/opencog/opencog/atomspace/SimpleTruthValue.h"
    "/opt/opencog/opencog/atomspace/SpaceServer.h"
    "/opt/opencog/opencog/atomspace/SpaceServerContainer.h"
    "/opt/opencog/opencog/atomspace/StatisticsMonitor.h"
    "/opt/opencog/opencog/atomspace/StringIndex.h"
    "/opt/opencog/opencog/atomspace/TargetTypeIndex.h"
    "/opt/opencog/opencog/atomspace/TemporalEntry.h"
    "/opt/opencog/opencog/atomspace/Temporal.h"
    "/opt/opencog/opencog/atomspace/TemporalMap.h"
    "/opt/opencog/opencog/atomspace/TemporalTable.h"
    "/opt/opencog/opencog/atomspace/TemporalToHandleSetMap.h"
    "/opt/opencog/opencog/atomspace/TimeServer.h"
    "/opt/opencog/opencog/atomspace/TLB.h"
    "/opt/opencog/opencog/atomspace/Trail.h"
    "/opt/opencog/opencog/atomspace/TruthValue.h"
    "/opt/opencog/opencog/atomspace/TypeIndex.h"
    "/opt/opencog/opencog/atomspace/VersionHandle.h"
    "/opt/opencog/opencog/atomspace/types.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

