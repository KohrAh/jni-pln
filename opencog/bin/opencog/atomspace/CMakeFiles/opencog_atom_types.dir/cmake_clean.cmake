FILE(REMOVE_RECURSE
  "CMakeFiles/opencog_atom_types"
  "../../../opencog/atomspace/atom_types.h"
  "../../../opencog/atomspace/atom_types.definitions"
  "../../../opencog/atomspace/atom_types.inheritance"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/opencog_atom_types.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
