# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/atomspace/Atom.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/Atom.cc.o"
  "/opt/opencog/opencog/atomspace/AtomSpace.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/AtomSpace.cc.o"
  "/opt/opencog/opencog/atomspace/AtomSpaceAsync.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/AtomSpaceAsync.cc.o"
  "/opt/opencog/opencog/atomspace/AtomSpaceImpl.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/AtomSpaceImpl.cc.o"
  "/opt/opencog/opencog/atomspace/AtomSpaceInit.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/AtomSpaceInit.cc.o"
  "/opt/opencog/opencog/atomspace/AtomTable.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/AtomTable.cc.o"
  "/opt/opencog/opencog/atomspace/AttentionBank.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/AttentionBank.cc.o"
  "/opt/opencog/opencog/atomspace/AttentionValue.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/AttentionValue.cc.o"
  "/opt/opencog/opencog/atomspace/ClassServer.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/ClassServer.cc.o"
  "/opt/opencog/opencog/atomspace/CompositeTruthValue.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/CompositeTruthValue.cc.o"
  "/opt/opencog/opencog/atomspace/CountTruthValue.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/CountTruthValue.cc.o"
  "/opt/opencog/opencog/atomspace/FixedIntegerIndex.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/FixedIntegerIndex.cc.o"
  "/opt/opencog/opencog/atomspace/Handle.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/Handle.cc.o"
  "/opt/opencog/opencog/atomspace/HandleEntry.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/HandleEntry.cc.o"
  "/opt/opencog/opencog/atomspace/HandleIterator.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/HandleIterator.cc.o"
  "/opt/opencog/opencog/atomspace/HandleSeqIndex.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/HandleSeqIndex.cc.o"
  "/opt/opencog/opencog/atomspace/HandleSet.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/HandleSet.cc.o"
  "/opt/opencog/opencog/atomspace/HandleTemporalPair.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/HandleTemporalPair.cc.o"
  "/opt/opencog/opencog/atomspace/HandleTemporalPairEntry.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/HandleTemporalPairEntry.cc.o"
  "/opt/opencog/opencog/atomspace/HandleToTemporalEntryMap.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/HandleToTemporalEntryMap.cc.o"
  "/opt/opencog/opencog/atomspace/ImportanceIndex.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/ImportanceIndex.cc.o"
  "/opt/opencog/opencog/atomspace/IndefiniteTruthValue.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/IndefiniteTruthValue.cc.o"
  "/opt/opencog/opencog/atomspace/Link.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/Link.cc.o"
  "/opt/opencog/opencog/atomspace/LinkIndex.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/LinkIndex.cc.o"
  "/opt/opencog/opencog/atomspace/NameIndex.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/NameIndex.cc.o"
  "/opt/opencog/opencog/atomspace/Node.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/Node.cc.o"
  "/opt/opencog/opencog/atomspace/NodeIndex.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/NodeIndex.cc.o"
  "/opt/opencog/opencog/atomspace/NullTruthValue.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/NullTruthValue.cc.o"
  "/opt/opencog/opencog/atomspace/PredicateIndex.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/PredicateIndex.cc.o"
  "/opt/opencog/opencog/atomspace/SimpleTruthValue.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/SimpleTruthValue.cc.o"
  "/opt/opencog/opencog/atomspace/SpaceServer.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/SpaceServer.cc.o"
  "/opt/opencog/opencog/atomspace/StatisticsMonitor.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/StatisticsMonitor.cc.o"
  "/opt/opencog/opencog/atomspace/StringIndex.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/StringIndex.cc.o"
  "/opt/opencog/opencog/atomspace/TLB.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/TLB.cc.o"
  "/opt/opencog/opencog/atomspace/TargetTypeIndex.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/TargetTypeIndex.cc.o"
  "/opt/opencog/opencog/atomspace/Temporal.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/Temporal.cc.o"
  "/opt/opencog/opencog/atomspace/TemporalEntry.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/TemporalEntry.cc.o"
  "/opt/opencog/opencog/atomspace/TemporalMap.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/TemporalMap.cc.o"
  "/opt/opencog/opencog/atomspace/TemporalTable.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/TemporalTable.cc.o"
  "/opt/opencog/opencog/atomspace/TemporalToHandleSetMap.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/TemporalToHandleSetMap.cc.o"
  "/opt/opencog/opencog/atomspace/TimeServer.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/TimeServer.cc.o"
  "/opt/opencog/opencog/atomspace/Trail.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/Trail.cc.o"
  "/opt/opencog/opencog/atomspace/TruthValue.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/TruthValue.cc.o"
  "/opt/opencog/opencog/atomspace/TypeIndex.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/TypeIndex.cc.o"
  "/opt/opencog/opencog/atomspace/VersionHandle.cc" "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/VersionHandle.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Pairs of files generated by the same build rule.
SET(CMAKE_MULTIPLE_OUTPUT_PAIRS
  "/opt/opencog/opencog/atomspace/atom_types.definitions" "/opt/opencog/opencog/atomspace/atom_types.h"
  "/opt/opencog/opencog/atomspace/atom_types.inheritance" "/opt/opencog/opencog/atomspace/atom_types.h"
  )


# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opt/opencog/bin/opencog/persist/xml/CMakeFiles/xml.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/DependInfo.cmake"
  )
