FILE(REMOVE_RECURSE
  "CMakeFiles/atomspace.dir/Atom.cc.o"
  "CMakeFiles/atomspace.dir/AtomSpace.cc.o"
  "CMakeFiles/atomspace.dir/AtomSpaceAsync.cc.o"
  "CMakeFiles/atomspace.dir/AtomSpaceImpl.cc.o"
  "CMakeFiles/atomspace.dir/AtomSpaceInit.cc.o"
  "CMakeFiles/atomspace.dir/AtomTable.cc.o"
  "CMakeFiles/atomspace.dir/AttentionValue.cc.o"
  "CMakeFiles/atomspace.dir/AttentionBank.cc.o"
  "CMakeFiles/atomspace.dir/ClassServer.cc.o"
  "CMakeFiles/atomspace.dir/CompositeTruthValue.cc.o"
  "CMakeFiles/atomspace.dir/CountTruthValue.cc.o"
  "CMakeFiles/atomspace.dir/FixedIntegerIndex.cc.o"
  "CMakeFiles/atomspace.dir/Handle.cc.o"
  "CMakeFiles/atomspace.dir/HandleEntry.cc.o"
  "CMakeFiles/atomspace.dir/HandleIterator.cc.o"
  "CMakeFiles/atomspace.dir/HandleSeqIndex.cc.o"
  "CMakeFiles/atomspace.dir/HandleSet.cc.o"
  "CMakeFiles/atomspace.dir/HandleTemporalPair.cc.o"
  "CMakeFiles/atomspace.dir/HandleTemporalPairEntry.cc.o"
  "CMakeFiles/atomspace.dir/HandleToTemporalEntryMap.cc.o"
  "CMakeFiles/atomspace.dir/ImportanceIndex.cc.o"
  "CMakeFiles/atomspace.dir/IndefiniteTruthValue.cc.o"
  "CMakeFiles/atomspace.dir/Link.cc.o"
  "CMakeFiles/atomspace.dir/LinkIndex.cc.o"
  "CMakeFiles/atomspace.dir/NameIndex.cc.o"
  "CMakeFiles/atomspace.dir/Node.cc.o"
  "CMakeFiles/atomspace.dir/NodeIndex.cc.o"
  "CMakeFiles/atomspace.dir/NullTruthValue.cc.o"
  "CMakeFiles/atomspace.dir/PredicateIndex.cc.o"
  "CMakeFiles/atomspace.dir/SimpleTruthValue.cc.o"
  "CMakeFiles/atomspace.dir/SpaceServer.cc.o"
  "CMakeFiles/atomspace.dir/StatisticsMonitor.cc.o"
  "CMakeFiles/atomspace.dir/StringIndex.cc.o"
  "CMakeFiles/atomspace.dir/TargetTypeIndex.cc.o"
  "CMakeFiles/atomspace.dir/Temporal.cc.o"
  "CMakeFiles/atomspace.dir/TemporalEntry.cc.o"
  "CMakeFiles/atomspace.dir/TemporalMap.cc.o"
  "CMakeFiles/atomspace.dir/TemporalTable.cc.o"
  "CMakeFiles/atomspace.dir/TemporalToHandleSetMap.cc.o"
  "CMakeFiles/atomspace.dir/TimeServer.cc.o"
  "CMakeFiles/atomspace.dir/TLB.cc.o"
  "CMakeFiles/atomspace.dir/Trail.cc.o"
  "CMakeFiles/atomspace.dir/TruthValue.cc.o"
  "CMakeFiles/atomspace.dir/TypeIndex.cc.o"
  "CMakeFiles/atomspace.dir/VersionHandle.cc.o"
  "../../../opencog/atomspace/atom_types.h"
  "../../../opencog/atomspace/atom_types.definitions"
  "../../../opencog/atomspace/atom_types.inheritance"
  "libatomspace.pdb"
  "libatomspace.so"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang CXX)
  INCLUDE(CMakeFiles/atomspace.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
