# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canoncical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /opt/opencog

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /opt/opencog/bin

# Include any dependencies generated for this target.
include opencog/atomspace/CMakeFiles/atomspace_async.dir/depend.make

# Include the progress variables for this target.
include opencog/atomspace/CMakeFiles/atomspace_async.dir/progress.make

# Include the compile flags for this target's objects.
include opencog/atomspace/CMakeFiles/atomspace_async.dir/flags.make

opencog/atomspace/CMakeFiles/atomspace_async.dir/atomspace_async_tester.cc.o: opencog/atomspace/CMakeFiles/atomspace_async.dir/flags.make
opencog/atomspace/CMakeFiles/atomspace_async.dir/atomspace_async_tester.cc.o: ../opencog/atomspace/atomspace_async_tester.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/atomspace/CMakeFiles/atomspace_async.dir/atomspace_async_tester.cc.o"
	cd /opt/opencog/bin/opencog/atomspace && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/atomspace_async.dir/atomspace_async_tester.cc.o -c /opt/opencog/opencog/atomspace/atomspace_async_tester.cc

opencog/atomspace/CMakeFiles/atomspace_async.dir/atomspace_async_tester.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/atomspace_async.dir/atomspace_async_tester.cc.i"
	cd /opt/opencog/bin/opencog/atomspace && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/atomspace/atomspace_async_tester.cc > CMakeFiles/atomspace_async.dir/atomspace_async_tester.cc.i

opencog/atomspace/CMakeFiles/atomspace_async.dir/atomspace_async_tester.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/atomspace_async.dir/atomspace_async_tester.cc.s"
	cd /opt/opencog/bin/opencog/atomspace && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/atomspace/atomspace_async_tester.cc -o CMakeFiles/atomspace_async.dir/atomspace_async_tester.cc.s

opencog/atomspace/CMakeFiles/atomspace_async.dir/atomspace_async_tester.cc.o.requires:
.PHONY : opencog/atomspace/CMakeFiles/atomspace_async.dir/atomspace_async_tester.cc.o.requires

opencog/atomspace/CMakeFiles/atomspace_async.dir/atomspace_async_tester.cc.o.provides: opencog/atomspace/CMakeFiles/atomspace_async.dir/atomspace_async_tester.cc.o.requires
	$(MAKE) -f opencog/atomspace/CMakeFiles/atomspace_async.dir/build.make opencog/atomspace/CMakeFiles/atomspace_async.dir/atomspace_async_tester.cc.o.provides.build
.PHONY : opencog/atomspace/CMakeFiles/atomspace_async.dir/atomspace_async_tester.cc.o.provides

opencog/atomspace/CMakeFiles/atomspace_async.dir/atomspace_async_tester.cc.o.provides.build: opencog/atomspace/CMakeFiles/atomspace_async.dir/atomspace_async_tester.cc.o

# Object files for target atomspace_async
atomspace_async_OBJECTS = \
"CMakeFiles/atomspace_async.dir/atomspace_async_tester.cc.o"

# External object files for target atomspace_async
atomspace_async_EXTERNAL_OBJECTS =

opencog/atomspace/atomspace_async: opencog/atomspace/CMakeFiles/atomspace_async.dir/atomspace_async_tester.cc.o
opencog/atomspace/atomspace_async: opencog/atomspace/libatomspace.so
opencog/atomspace/atomspace_async: opencog/util/libutil.so
opencog/atomspace/atomspace_async: /usr/lib/libboost_filesystem-mt.so
opencog/atomspace/atomspace_async: /usr/lib/libboost_system-mt.so
opencog/atomspace/atomspace_async: /usr/lib/libboost_regex-mt.so
opencog/atomspace/atomspace_async: opencog/persist/xml/libxml.so
opencog/atomspace/atomspace_async: /usr/lib/i386-linux-gnu/libexpat.so
opencog/atomspace/atomspace_async: opencog/spatial/libSpaceMap.so
opencog/atomspace/atomspace_async: /usr/lib/libboost_signals-mt.so
opencog/atomspace/atomspace_async: /usr/lib/libboost_thread-mt.so
opencog/atomspace/atomspace_async: opencog/atomspace/CMakeFiles/atomspace_async.dir/build.make
opencog/atomspace/atomspace_async: opencog/atomspace/CMakeFiles/atomspace_async.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable atomspace_async"
	cd /opt/opencog/bin/opencog/atomspace && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/atomspace_async.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
opencog/atomspace/CMakeFiles/atomspace_async.dir/build: opencog/atomspace/atomspace_async
.PHONY : opencog/atomspace/CMakeFiles/atomspace_async.dir/build

opencog/atomspace/CMakeFiles/atomspace_async.dir/requires: opencog/atomspace/CMakeFiles/atomspace_async.dir/atomspace_async_tester.cc.o.requires
.PHONY : opencog/atomspace/CMakeFiles/atomspace_async.dir/requires

opencog/atomspace/CMakeFiles/atomspace_async.dir/clean:
	cd /opt/opencog/bin/opencog/atomspace && $(CMAKE_COMMAND) -P CMakeFiles/atomspace_async.dir/cmake_clean.cmake
.PHONY : opencog/atomspace/CMakeFiles/atomspace_async.dir/clean

opencog/atomspace/CMakeFiles/atomspace_async.dir/depend:
	cd /opt/opencog/bin && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /opt/opencog /opt/opencog/opencog/atomspace /opt/opencog/bin /opt/opencog/bin/opencog/atomspace /opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace_async.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : opencog/atomspace/CMakeFiles/atomspace_async.dir/depend

