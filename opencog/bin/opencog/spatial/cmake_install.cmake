# Install script for directory: /opt/opencog/opencog/spatial

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Debug")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/opencog/spatial" TYPE FILE FILES
    "/opt/opencog/opencog/spatial/Agent.h"
    "/opt/opencog/opencog/spatial/AStarController.h"
    "/opt/opencog/opencog/spatial/AStar3DController.h"
    "/opt/opencog/opencog/spatial/Entity.h"
    "/opt/opencog/opencog/spatial/fsa.h"
    "/opt/opencog/opencog/spatial/HPASearch.h"
    "/opt/opencog/opencog/spatial/HumanoidAgent.h"
    "/opt/opencog/opencog/spatial/LocalSpaceMap2D.h"
    "/opt/opencog/opencog/spatial/LocalSpaceMap2DUtil.h"
    "/opt/opencog/opencog/spatial/LSMap2DSearchNode.h"
    "/opt/opencog/opencog/spatial/LSMap3DSearchNode.h"
    "/opt/opencog/opencog/spatial/MapExplorer.h"
    "/opt/opencog/opencog/spatial/MapExplorerServer.h"
    "/opt/opencog/opencog/spatial/MovableEntity.h"
    "/opt/opencog/opencog/spatial/PetAgent.h"
    "/opt/opencog/opencog/spatial/Prerequisites.h"
    "/opt/opencog/opencog/spatial/QuadTree.h"
    "/opt/opencog/opencog/spatial/Block.h"
    "/opt/opencog/opencog/spatial/StaticEntity.h"
    "/opt/opencog/opencog/spatial/stlastar.h"
    "/opt/opencog/opencog/spatial/SuperEntity.h"
    "/opt/opencog/opencog/spatial/VisibilityMap.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/opencog/spatial/math" TYPE FILE FILES
    "/opt/opencog/opencog/spatial/math/BoundingBox.h"
    "/opt/opencog/opencog/spatial/math/Dimension2.h"
    "/opt/opencog/opencog/spatial/math/Dimension3.h"
    "/opt/opencog/opencog/spatial/math/Face.h"
    "/opt/opencog/opencog/spatial/math/Line.h"
    "/opt/opencog/opencog/spatial/math/LineSegment.h"
    "/opt/opencog/opencog/spatial/math/Matrix3.h"
    "/opt/opencog/opencog/spatial/math/Matrix4.h"
    "/opt/opencog/opencog/spatial/math/Plane.h"
    "/opt/opencog/opencog/spatial/math/Quaternion.h"
    "/opt/opencog/opencog/spatial/math/Rectangle.h"
    "/opt/opencog/opencog/spatial/math/SquareFace.h"
    "/opt/opencog/opencog/spatial/math/Triangle.h"
    "/opt/opencog/opencog/spatial/math/Vector2.h"
    "/opt/opencog/opencog/spatial/math/Vector3.h"
    "/opt/opencog/opencog/spatial/math/Vector4.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

