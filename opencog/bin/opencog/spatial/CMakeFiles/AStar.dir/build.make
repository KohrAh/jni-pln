# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canoncical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /opt/opencog

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /opt/opencog/bin

# Include any dependencies generated for this target.
include opencog/spatial/CMakeFiles/AStar.dir/depend.make

# Include the progress variables for this target.
include opencog/spatial/CMakeFiles/AStar.dir/progress.make

# Include the compile flags for this target's objects.
include opencog/spatial/CMakeFiles/AStar.dir/flags.make

opencog/spatial/CMakeFiles/AStar.dir/AStarController.cc.o: opencog/spatial/CMakeFiles/AStar.dir/flags.make
opencog/spatial/CMakeFiles/AStar.dir/AStarController.cc.o: ../opencog/spatial/AStarController.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/spatial/CMakeFiles/AStar.dir/AStarController.cc.o"
	cd /opt/opencog/bin/opencog/spatial && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/AStar.dir/AStarController.cc.o -c /opt/opencog/opencog/spatial/AStarController.cc

opencog/spatial/CMakeFiles/AStar.dir/AStarController.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/AStar.dir/AStarController.cc.i"
	cd /opt/opencog/bin/opencog/spatial && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/spatial/AStarController.cc > CMakeFiles/AStar.dir/AStarController.cc.i

opencog/spatial/CMakeFiles/AStar.dir/AStarController.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/AStar.dir/AStarController.cc.s"
	cd /opt/opencog/bin/opencog/spatial && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/spatial/AStarController.cc -o CMakeFiles/AStar.dir/AStarController.cc.s

opencog/spatial/CMakeFiles/AStar.dir/AStarController.cc.o.requires:
.PHONY : opencog/spatial/CMakeFiles/AStar.dir/AStarController.cc.o.requires

opencog/spatial/CMakeFiles/AStar.dir/AStarController.cc.o.provides: opencog/spatial/CMakeFiles/AStar.dir/AStarController.cc.o.requires
	$(MAKE) -f opencog/spatial/CMakeFiles/AStar.dir/build.make opencog/spatial/CMakeFiles/AStar.dir/AStarController.cc.o.provides.build
.PHONY : opencog/spatial/CMakeFiles/AStar.dir/AStarController.cc.o.provides

opencog/spatial/CMakeFiles/AStar.dir/AStarController.cc.o.provides.build: opencog/spatial/CMakeFiles/AStar.dir/AStarController.cc.o

opencog/spatial/CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.o: opencog/spatial/CMakeFiles/AStar.dir/flags.make
opencog/spatial/CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.o: ../opencog/spatial/LSMap2DSearchNode.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/spatial/CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.o"
	cd /opt/opencog/bin/opencog/spatial && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.o -c /opt/opencog/opencog/spatial/LSMap2DSearchNode.cc

opencog/spatial/CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.i"
	cd /opt/opencog/bin/opencog/spatial && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/spatial/LSMap2DSearchNode.cc > CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.i

opencog/spatial/CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.s"
	cd /opt/opencog/bin/opencog/spatial && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/spatial/LSMap2DSearchNode.cc -o CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.s

opencog/spatial/CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.o.requires:
.PHONY : opencog/spatial/CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.o.requires

opencog/spatial/CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.o.provides: opencog/spatial/CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.o.requires
	$(MAKE) -f opencog/spatial/CMakeFiles/AStar.dir/build.make opencog/spatial/CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.o.provides.build
.PHONY : opencog/spatial/CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.o.provides

opencog/spatial/CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.o.provides.build: opencog/spatial/CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.o

opencog/spatial/CMakeFiles/AStar.dir/AStar3DController.cc.o: opencog/spatial/CMakeFiles/AStar.dir/flags.make
opencog/spatial/CMakeFiles/AStar.dir/AStar3DController.cc.o: ../opencog/spatial/AStar3DController.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_3)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/spatial/CMakeFiles/AStar.dir/AStar3DController.cc.o"
	cd /opt/opencog/bin/opencog/spatial && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/AStar.dir/AStar3DController.cc.o -c /opt/opencog/opencog/spatial/AStar3DController.cc

opencog/spatial/CMakeFiles/AStar.dir/AStar3DController.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/AStar.dir/AStar3DController.cc.i"
	cd /opt/opencog/bin/opencog/spatial && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/spatial/AStar3DController.cc > CMakeFiles/AStar.dir/AStar3DController.cc.i

opencog/spatial/CMakeFiles/AStar.dir/AStar3DController.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/AStar.dir/AStar3DController.cc.s"
	cd /opt/opencog/bin/opencog/spatial && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/spatial/AStar3DController.cc -o CMakeFiles/AStar.dir/AStar3DController.cc.s

opencog/spatial/CMakeFiles/AStar.dir/AStar3DController.cc.o.requires:
.PHONY : opencog/spatial/CMakeFiles/AStar.dir/AStar3DController.cc.o.requires

opencog/spatial/CMakeFiles/AStar.dir/AStar3DController.cc.o.provides: opencog/spatial/CMakeFiles/AStar.dir/AStar3DController.cc.o.requires
	$(MAKE) -f opencog/spatial/CMakeFiles/AStar.dir/build.make opencog/spatial/CMakeFiles/AStar.dir/AStar3DController.cc.o.provides.build
.PHONY : opencog/spatial/CMakeFiles/AStar.dir/AStar3DController.cc.o.provides

opencog/spatial/CMakeFiles/AStar.dir/AStar3DController.cc.o.provides.build: opencog/spatial/CMakeFiles/AStar.dir/AStar3DController.cc.o

opencog/spatial/CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.o: opencog/spatial/CMakeFiles/AStar.dir/flags.make
opencog/spatial/CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.o: ../opencog/spatial/LSMap3DSearchNode.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_4)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/spatial/CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.o"
	cd /opt/opencog/bin/opencog/spatial && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.o -c /opt/opencog/opencog/spatial/LSMap3DSearchNode.cc

opencog/spatial/CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.i"
	cd /opt/opencog/bin/opencog/spatial && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/spatial/LSMap3DSearchNode.cc > CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.i

opencog/spatial/CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.s"
	cd /opt/opencog/bin/opencog/spatial && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/spatial/LSMap3DSearchNode.cc -o CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.s

opencog/spatial/CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.o.requires:
.PHONY : opencog/spatial/CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.o.requires

opencog/spatial/CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.o.provides: opencog/spatial/CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.o.requires
	$(MAKE) -f opencog/spatial/CMakeFiles/AStar.dir/build.make opencog/spatial/CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.o.provides.build
.PHONY : opencog/spatial/CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.o.provides

opencog/spatial/CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.o.provides.build: opencog/spatial/CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.o

# Object files for target AStar
AStar_OBJECTS = \
"CMakeFiles/AStar.dir/AStarController.cc.o" \
"CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.o" \
"CMakeFiles/AStar.dir/AStar3DController.cc.o" \
"CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.o"

# External object files for target AStar
AStar_EXTERNAL_OBJECTS =

opencog/spatial/libAStar.a: opencog/spatial/CMakeFiles/AStar.dir/AStarController.cc.o
opencog/spatial/libAStar.a: opencog/spatial/CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.o
opencog/spatial/libAStar.a: opencog/spatial/CMakeFiles/AStar.dir/AStar3DController.cc.o
opencog/spatial/libAStar.a: opencog/spatial/CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.o
opencog/spatial/libAStar.a: opencog/spatial/CMakeFiles/AStar.dir/build.make
opencog/spatial/libAStar.a: opencog/spatial/CMakeFiles/AStar.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX static library libAStar.a"
	cd /opt/opencog/bin/opencog/spatial && $(CMAKE_COMMAND) -P CMakeFiles/AStar.dir/cmake_clean_target.cmake
	cd /opt/opencog/bin/opencog/spatial && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/AStar.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
opencog/spatial/CMakeFiles/AStar.dir/build: opencog/spatial/libAStar.a
.PHONY : opencog/spatial/CMakeFiles/AStar.dir/build

opencog/spatial/CMakeFiles/AStar.dir/requires: opencog/spatial/CMakeFiles/AStar.dir/AStarController.cc.o.requires
opencog/spatial/CMakeFiles/AStar.dir/requires: opencog/spatial/CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.o.requires
opencog/spatial/CMakeFiles/AStar.dir/requires: opencog/spatial/CMakeFiles/AStar.dir/AStar3DController.cc.o.requires
opencog/spatial/CMakeFiles/AStar.dir/requires: opencog/spatial/CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.o.requires
.PHONY : opencog/spatial/CMakeFiles/AStar.dir/requires

opencog/spatial/CMakeFiles/AStar.dir/clean:
	cd /opt/opencog/bin/opencog/spatial && $(CMAKE_COMMAND) -P CMakeFiles/AStar.dir/cmake_clean.cmake
.PHONY : opencog/spatial/CMakeFiles/AStar.dir/clean

opencog/spatial/CMakeFiles/AStar.dir/depend:
	cd /opt/opencog/bin && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /opt/opencog /opt/opencog/opencog/spatial /opt/opencog/bin /opt/opencog/bin/opencog/spatial /opt/opencog/bin/opencog/spatial/CMakeFiles/AStar.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : opencog/spatial/CMakeFiles/AStar.dir/depend

