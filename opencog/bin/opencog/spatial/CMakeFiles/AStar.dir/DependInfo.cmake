# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/spatial/AStar3DController.cc" "/opt/opencog/bin/opencog/spatial/CMakeFiles/AStar.dir/AStar3DController.cc.o"
  "/opt/opencog/opencog/spatial/AStarController.cc" "/opt/opencog/bin/opencog/spatial/CMakeFiles/AStar.dir/AStarController.cc.o"
  "/opt/opencog/opencog/spatial/LSMap2DSearchNode.cc" "/opt/opencog/bin/opencog/spatial/CMakeFiles/AStar.dir/LSMap2DSearchNode.cc.o"
  "/opt/opencog/opencog/spatial/LSMap3DSearchNode.cc" "/opt/opencog/bin/opencog/spatial/CMakeFiles/AStar.dir/LSMap3DSearchNode.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )
