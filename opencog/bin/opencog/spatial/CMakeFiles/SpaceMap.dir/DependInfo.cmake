# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/spatial/Block.cc" "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/Block.cc.o"
  "/opt/opencog/opencog/spatial/Entity.cc" "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/Entity.cc.o"
  "/opt/opencog/opencog/spatial/LocalSpaceMap2D.cc" "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/LocalSpaceMap2D.cc.o"
  "/opt/opencog/opencog/spatial/LocalSpaceMap2DUtil.cc" "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/LocalSpaceMap2DUtil.cc.o"
  "/opt/opencog/opencog/spatial/MapExplorerServer.cc" "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/MapExplorerServer.cc.o"
  "/opt/opencog/opencog/spatial/MovableEntity.cc" "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/MovableEntity.cc.o"
  "/opt/opencog/opencog/spatial/SuperEntity.cc" "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/SuperEntity.cc.o"
  "/opt/opencog/opencog/spatial/VisibilityMap.cc" "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/VisibilityMap.cc.o"
  "/opt/opencog/opencog/spatial/math/BoundingBox.cc" "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/math/BoundingBox.cc.o"
  "/opt/opencog/opencog/spatial/math/Face.cc" "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/math/Face.cc.o"
  "/opt/opencog/opencog/spatial/math/MathCommon.cc" "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/math/MathCommon.cc.o"
  "/opt/opencog/opencog/spatial/math/Matrix3.cc" "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/math/Matrix3.cc.o"
  "/opt/opencog/opencog/spatial/math/Matrix4.cc" "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/math/Matrix4.cc.o"
  "/opt/opencog/opencog/spatial/math/Plane.cc" "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/math/Plane.cc.o"
  "/opt/opencog/opencog/spatial/math/Quaternion.cc" "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/math/Quaternion.cc.o"
  "/opt/opencog/opencog/spatial/math/Rectangle.cc" "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/math/Rectangle.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )
