# Install script for directory: /opt/opencog/opencog/reasoning/pln

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Debug")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libpln.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libpln.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libpln.so"
         RPATH "/usr/local/lib/opencog")
  ENDIF()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/opencog" TYPE SHARED_LIBRARY FILES "/opt/opencog/bin/opencog/reasoning/pln/libpln.so")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libpln.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libpln.so")
    FILE(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libpln.so"
         OLD_RPATH "/opt/opencog/bin/opencog/ubigraph:/opt/opencog/bin/opencog/persist/xml:/opt/opencog/bin/opencog/guile:/opt/opencog/bin/opencog/atomspace:/opt/opencog/bin/opencog/util:/opt/opencog/bin/opencog/spatial:"
         NEW_RPATH "/usr/local/lib/opencog")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/opencog/libpln.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/opencog/reasoning/pln" TYPE FILE FILES
    "/opt/opencog/opencog/reasoning/pln/PLNUtils.h"
    "/opt/opencog/opencog/reasoning/pln/PLNModule.h"
    "/opt/opencog/opencog/reasoning/pln/AtomLookupProvider.h"
    "/opt/opencog/opencog/reasoning/pln/PLNatom.h"
    "/opt/opencog/opencog/reasoning/pln/PLN.h"
    "/opt/opencog/opencog/reasoning/pln/BackInferenceTreeNode.h"
    "/opt/opencog/opencog/reasoning/pln/InferenceCache.h"
    "/opt/opencog/opencog/reasoning/pln/TestTargets.h"
    "/opt/opencog/opencog/reasoning/pln/BackChainingAgent.h"
    "/opt/opencog/opencog/reasoning/pln/ForwardChainingAgent.h"
    "/opt/opencog/opencog/reasoning/pln/rules/Rule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/implication/ImplicationRedundantExpansionRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/implication/ImplicationBreakdownRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/implication/ImplicationConstructionRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/implication/StrictImplicationBreakdownRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/implication/ImplicationTailExpansionRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/substitution/BaseSubstRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/substitution/InheritanceSubstRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/substitution/SimilaritySubstRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/or/OrRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/or/Or2AndRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/or/OrPartitionRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/RuleApp.h"
    "/opt/opencog/opencog/reasoning/pln/rules/inference/DeductionRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/inference/HypothesisRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/inference/InversionRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/inference/RevisionRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/inference/QuantifierRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/not/NotRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/not/NotEliminationRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/auxiliary/LookupRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/auxiliary/VarInstantiationRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/auxiliary/ScholemFunctionProductionRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/auxiliary/SubsetEvalRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/auxiliary/PrintRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/auxiliary/UnorderedLinkPermutationRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/auxiliary/CrispTheoremRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/convert/Ext2ExtRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/convert/Inh2EvalRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/convert/Sim2InhRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/convert/Inh2SimRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/convert/Exist2ForAllRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/convert/Eval2MemRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/convert/Link2LinkRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/convert/Equi2ImpRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/convert/Inclusion2EvalRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/RuleProvider.h"
    "/opt/opencog/opencog/reasoning/pln/rules/RuleFunctions.h"
    "/opt/opencog/opencog/reasoning/pln/rules/GenericRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/instantiation/BaseInstantiationRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/instantiation/ForAllInstantiationRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/instantiation/AverageInstantiationRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/Rules.h"
    "/opt/opencog/opencog/reasoning/pln/rules/and/And2OrRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/and/AndBreakdownRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/and/AndSubstRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/and/AndRuleArityFree.h"
    "/opt/opencog/opencog/reasoning/pln/rules/and/AndRuleSimple.h"
    "/opt/opencog/opencog/reasoning/pln/rules/and/AndRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/and/AndPartitionRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/context/ContextualizerRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/context/DecontextualizerRule.h"
    "/opt/opencog/opencog/reasoning/pln/rules/context/ContextFreeToSensitiveRule.h"
    "/opt/opencog/opencog/reasoning/pln/AtomSpaceWrapper.h"
    "/opt/opencog/opencog/reasoning/pln/utils/fim.h"
    "/opt/opencog/opencog/reasoning/pln/utils/Singleton.h"
    "/opt/opencog/opencog/reasoning/pln/utils/NMPrinter.h"
    "/opt/opencog/opencog/reasoning/pln/ForwardChainer.h"
    "/opt/opencog/opencog/reasoning/pln/formulas/VectorLookupTable.h"
    "/opt/opencog/opencog/reasoning/pln/formulas/FormulasIndefinite.h"
    "/opt/opencog/opencog/reasoning/pln/formulas/LookupTable.h"
    "/opt/opencog/opencog/reasoning/pln/formulas/Formula.h"
    "/opt/opencog/opencog/reasoning/pln/formulas/DeductionLookupTable.h"
    "/opt/opencog/opencog/reasoning/pln/formulas/Formulas.h"
    "/opt/opencog/opencog/reasoning/pln/iAtomSpaceWrapper.h"
    "/opt/opencog/opencog/reasoning/pln/PLNEvaluator.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

