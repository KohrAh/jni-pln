# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/reasoning/pln/ASSOC.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/ASSOC.cc.o"
  "/opt/opencog/opencog/reasoning/pln/AtomSpaceWrapper.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/AtomSpaceWrapper.cc.o"
  "/opt/opencog/opencog/reasoning/pln/BackChainingAgent.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/BackChainingAgent.cc.o"
  "/opt/opencog/opencog/reasoning/pln/BackInferenceTreeNode.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/BackInferenceTreeNode.cc.o"
  "/opt/opencog/opencog/reasoning/pln/ForwardChainer.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/ForwardChainer.cc.o"
  "/opt/opencog/opencog/reasoning/pln/ForwardChainingAgent.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/ForwardChainingAgent.cc.o"
  "/opt/opencog/opencog/reasoning/pln/InferenceCache.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/InferenceCache.cc.o"
  "/opt/opencog/opencog/reasoning/pln/PLNGlobals.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/PLNGlobals.cc.o"
  "/opt/opencog/opencog/reasoning/pln/PLNModule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/PLNModule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/PLNUtils.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/PLNUtils.cc.o"
  "/opt/opencog/opencog/reasoning/pln/PLNatom.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/PLNatom.cc.o"
  "/opt/opencog/opencog/reasoning/pln/Testing.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/Testing.cc.o"
  "/opt/opencog/opencog/reasoning/pln/formulas/DeductionLookupTable.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/formulas/DeductionLookupTable.cc.o"
  "/opt/opencog/opencog/reasoning/pln/formulas/Formulas.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/formulas/Formulas.cc.o"
  "/opt/opencog/opencog/reasoning/pln/formulas/FormulasIndefinite.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/formulas/FormulasIndefinite.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/Rule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/Rule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/RuleApp.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/RuleApp.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/RuleFunctions.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/RuleFunctions.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/RuleProvider.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/RuleProvider.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/and/AndBreakdownRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/and/AndBreakdownRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/and/AndPartitionRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/and/AndPartitionRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/and/AndRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/and/AndRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/and/AndRuleArityFree.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/and/AndRuleArityFree.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/and/AndSubstRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/and/AndSubstRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/auxiliary/CrispTheoremRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/auxiliary/CrispTheoremRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/auxiliary/IntensionalInheritanceRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/auxiliary/IntensionalInheritanceRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/auxiliary/LookupRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/auxiliary/LookupRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/auxiliary/ScholemFunctionProductionRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/auxiliary/ScholemFunctionProductionRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/auxiliary/SubsetEvalRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/auxiliary/SubsetEvalRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/auxiliary/UnorderedLinkPermutationRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/auxiliary/UnorderedLinkPermutationRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/context/ContextFreeToSensitiveRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/context/ContextFreeToSensitiveRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/context/ContextualizerRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/context/ContextualizerRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/context/DecontextualizerRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/context/DecontextualizerRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/convert/Equi2ImpRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/convert/Equi2ImpRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/implication/ImplicationBreakdownRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/implication/ImplicationBreakdownRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/implication/ImplicationConstructionRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/implication/ImplicationConstructionRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/implication/ImplicationTailExpansionRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/implication/ImplicationTailExpansionRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/implication/StrictImplicationBreakdownRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/implication/StrictImplicationBreakdownRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/inference/HypothesisRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/inference/HypothesisRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/not/NotRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/not/NotRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/or/OrPartitionRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/or/OrPartitionRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/rules/or/OrRule.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/rules/or/OrRule.cc.o"
  "/opt/opencog/opencog/reasoning/pln/utils/Grim.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/utils/Grim.cc.o"
  "/opt/opencog/opencog/reasoning/pln/utils/NMPrinter.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/utils/NMPrinter.cc.o"
  "/opt/opencog/opencog/reasoning/pln/utils/mva.cc" "/opt/opencog/bin/opencog/reasoning/pln/CMakeFiles/pln.dir/utils/mva.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opt/opencog/bin/opencog/ubigraph/CMakeFiles/ubigraph.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/persist/xml/CMakeFiles/xml.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/guile/CMakeFiles/smob.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/DependInfo.cmake"
  )
