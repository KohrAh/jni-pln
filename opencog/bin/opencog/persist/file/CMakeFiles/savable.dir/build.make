# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canoncical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /opt/opencog

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /opt/opencog/bin

# Include any dependencies generated for this target.
include opencog/persist/file/CMakeFiles/savable.dir/depend.make

# Include the progress variables for this target.
include opencog/persist/file/CMakeFiles/savable.dir/progress.make

# Include the compile flags for this target's objects.
include opencog/persist/file/CMakeFiles/savable.dir/flags.make

opencog/persist/file/CMakeFiles/savable.dir/CoreUtils.cc.o: opencog/persist/file/CMakeFiles/savable.dir/flags.make
opencog/persist/file/CMakeFiles/savable.dir/CoreUtils.cc.o: ../opencog/persist/file/CoreUtils.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/persist/file/CMakeFiles/savable.dir/CoreUtils.cc.o"
	cd /opt/opencog/bin/opencog/persist/file && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/savable.dir/CoreUtils.cc.o -c /opt/opencog/opencog/persist/file/CoreUtils.cc

opencog/persist/file/CMakeFiles/savable.dir/CoreUtils.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/savable.dir/CoreUtils.cc.i"
	cd /opt/opencog/bin/opencog/persist/file && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/persist/file/CoreUtils.cc > CMakeFiles/savable.dir/CoreUtils.cc.i

opencog/persist/file/CMakeFiles/savable.dir/CoreUtils.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/savable.dir/CoreUtils.cc.s"
	cd /opt/opencog/bin/opencog/persist/file && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/persist/file/CoreUtils.cc -o CMakeFiles/savable.dir/CoreUtils.cc.s

opencog/persist/file/CMakeFiles/savable.dir/CoreUtils.cc.o.requires:
.PHONY : opencog/persist/file/CMakeFiles/savable.dir/CoreUtils.cc.o.requires

opencog/persist/file/CMakeFiles/savable.dir/CoreUtils.cc.o.provides: opencog/persist/file/CMakeFiles/savable.dir/CoreUtils.cc.o.requires
	$(MAKE) -f opencog/persist/file/CMakeFiles/savable.dir/build.make opencog/persist/file/CMakeFiles/savable.dir/CoreUtils.cc.o.provides.build
.PHONY : opencog/persist/file/CMakeFiles/savable.dir/CoreUtils.cc.o.provides

opencog/persist/file/CMakeFiles/savable.dir/CoreUtils.cc.o.provides.build: opencog/persist/file/CMakeFiles/savable.dir/CoreUtils.cc.o

opencog/persist/file/CMakeFiles/savable.dir/CompositeRenumber.cc.o: opencog/persist/file/CMakeFiles/savable.dir/flags.make
opencog/persist/file/CMakeFiles/savable.dir/CompositeRenumber.cc.o: ../opencog/persist/file/CompositeRenumber.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/persist/file/CMakeFiles/savable.dir/CompositeRenumber.cc.o"
	cd /opt/opencog/bin/opencog/persist/file && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/savable.dir/CompositeRenumber.cc.o -c /opt/opencog/opencog/persist/file/CompositeRenumber.cc

opencog/persist/file/CMakeFiles/savable.dir/CompositeRenumber.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/savable.dir/CompositeRenumber.cc.i"
	cd /opt/opencog/bin/opencog/persist/file && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/persist/file/CompositeRenumber.cc > CMakeFiles/savable.dir/CompositeRenumber.cc.i

opencog/persist/file/CMakeFiles/savable.dir/CompositeRenumber.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/savable.dir/CompositeRenumber.cc.s"
	cd /opt/opencog/bin/opencog/persist/file && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/persist/file/CompositeRenumber.cc -o CMakeFiles/savable.dir/CompositeRenumber.cc.s

opencog/persist/file/CMakeFiles/savable.dir/CompositeRenumber.cc.o.requires:
.PHONY : opencog/persist/file/CMakeFiles/savable.dir/CompositeRenumber.cc.o.requires

opencog/persist/file/CMakeFiles/savable.dir/CompositeRenumber.cc.o.provides: opencog/persist/file/CMakeFiles/savable.dir/CompositeRenumber.cc.o.requires
	$(MAKE) -f opencog/persist/file/CMakeFiles/savable.dir/build.make opencog/persist/file/CMakeFiles/savable.dir/CompositeRenumber.cc.o.provides.build
.PHONY : opencog/persist/file/CMakeFiles/savable.dir/CompositeRenumber.cc.o.provides

opencog/persist/file/CMakeFiles/savable.dir/CompositeRenumber.cc.o.provides.build: opencog/persist/file/CMakeFiles/savable.dir/CompositeRenumber.cc.o

opencog/persist/file/CMakeFiles/savable.dir/SavingLoading.cc.o: opencog/persist/file/CMakeFiles/savable.dir/flags.make
opencog/persist/file/CMakeFiles/savable.dir/SavingLoading.cc.o: ../opencog/persist/file/SavingLoading.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_3)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/persist/file/CMakeFiles/savable.dir/SavingLoading.cc.o"
	cd /opt/opencog/bin/opencog/persist/file && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/savable.dir/SavingLoading.cc.o -c /opt/opencog/opencog/persist/file/SavingLoading.cc

opencog/persist/file/CMakeFiles/savable.dir/SavingLoading.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/savable.dir/SavingLoading.cc.i"
	cd /opt/opencog/bin/opencog/persist/file && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/persist/file/SavingLoading.cc > CMakeFiles/savable.dir/SavingLoading.cc.i

opencog/persist/file/CMakeFiles/savable.dir/SavingLoading.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/savable.dir/SavingLoading.cc.s"
	cd /opt/opencog/bin/opencog/persist/file && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/persist/file/SavingLoading.cc -o CMakeFiles/savable.dir/SavingLoading.cc.s

opencog/persist/file/CMakeFiles/savable.dir/SavingLoading.cc.o.requires:
.PHONY : opencog/persist/file/CMakeFiles/savable.dir/SavingLoading.cc.o.requires

opencog/persist/file/CMakeFiles/savable.dir/SavingLoading.cc.o.provides: opencog/persist/file/CMakeFiles/savable.dir/SavingLoading.cc.o.requires
	$(MAKE) -f opencog/persist/file/CMakeFiles/savable.dir/build.make opencog/persist/file/CMakeFiles/savable.dir/SavingLoading.cc.o.provides.build
.PHONY : opencog/persist/file/CMakeFiles/savable.dir/SavingLoading.cc.o.provides

opencog/persist/file/CMakeFiles/savable.dir/SavingLoading.cc.o.provides.build: opencog/persist/file/CMakeFiles/savable.dir/SavingLoading.cc.o

opencog/persist/file/CMakeFiles/savable.dir/SpaceServerSavable.cc.o: opencog/persist/file/CMakeFiles/savable.dir/flags.make
opencog/persist/file/CMakeFiles/savable.dir/SpaceServerSavable.cc.o: ../opencog/persist/file/SpaceServerSavable.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_4)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/persist/file/CMakeFiles/savable.dir/SpaceServerSavable.cc.o"
	cd /opt/opencog/bin/opencog/persist/file && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/savable.dir/SpaceServerSavable.cc.o -c /opt/opencog/opencog/persist/file/SpaceServerSavable.cc

opencog/persist/file/CMakeFiles/savable.dir/SpaceServerSavable.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/savable.dir/SpaceServerSavable.cc.i"
	cd /opt/opencog/bin/opencog/persist/file && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/persist/file/SpaceServerSavable.cc > CMakeFiles/savable.dir/SpaceServerSavable.cc.i

opencog/persist/file/CMakeFiles/savable.dir/SpaceServerSavable.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/savable.dir/SpaceServerSavable.cc.s"
	cd /opt/opencog/bin/opencog/persist/file && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/persist/file/SpaceServerSavable.cc -o CMakeFiles/savable.dir/SpaceServerSavable.cc.s

opencog/persist/file/CMakeFiles/savable.dir/SpaceServerSavable.cc.o.requires:
.PHONY : opencog/persist/file/CMakeFiles/savable.dir/SpaceServerSavable.cc.o.requires

opencog/persist/file/CMakeFiles/savable.dir/SpaceServerSavable.cc.o.provides: opencog/persist/file/CMakeFiles/savable.dir/SpaceServerSavable.cc.o.requires
	$(MAKE) -f opencog/persist/file/CMakeFiles/savable.dir/build.make opencog/persist/file/CMakeFiles/savable.dir/SpaceServerSavable.cc.o.provides.build
.PHONY : opencog/persist/file/CMakeFiles/savable.dir/SpaceServerSavable.cc.o.provides

opencog/persist/file/CMakeFiles/savable.dir/SpaceServerSavable.cc.o.provides.build: opencog/persist/file/CMakeFiles/savable.dir/SpaceServerSavable.cc.o

opencog/persist/file/CMakeFiles/savable.dir/TimeServerSavable.cc.o: opencog/persist/file/CMakeFiles/savable.dir/flags.make
opencog/persist/file/CMakeFiles/savable.dir/TimeServerSavable.cc.o: ../opencog/persist/file/TimeServerSavable.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_5)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/persist/file/CMakeFiles/savable.dir/TimeServerSavable.cc.o"
	cd /opt/opencog/bin/opencog/persist/file && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/savable.dir/TimeServerSavable.cc.o -c /opt/opencog/opencog/persist/file/TimeServerSavable.cc

opencog/persist/file/CMakeFiles/savable.dir/TimeServerSavable.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/savable.dir/TimeServerSavable.cc.i"
	cd /opt/opencog/bin/opencog/persist/file && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/persist/file/TimeServerSavable.cc > CMakeFiles/savable.dir/TimeServerSavable.cc.i

opencog/persist/file/CMakeFiles/savable.dir/TimeServerSavable.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/savable.dir/TimeServerSavable.cc.s"
	cd /opt/opencog/bin/opencog/persist/file && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/persist/file/TimeServerSavable.cc -o CMakeFiles/savable.dir/TimeServerSavable.cc.s

opencog/persist/file/CMakeFiles/savable.dir/TimeServerSavable.cc.o.requires:
.PHONY : opencog/persist/file/CMakeFiles/savable.dir/TimeServerSavable.cc.o.requires

opencog/persist/file/CMakeFiles/savable.dir/TimeServerSavable.cc.o.provides: opencog/persist/file/CMakeFiles/savable.dir/TimeServerSavable.cc.o.requires
	$(MAKE) -f opencog/persist/file/CMakeFiles/savable.dir/build.make opencog/persist/file/CMakeFiles/savable.dir/TimeServerSavable.cc.o.provides.build
.PHONY : opencog/persist/file/CMakeFiles/savable.dir/TimeServerSavable.cc.o.provides

opencog/persist/file/CMakeFiles/savable.dir/TimeServerSavable.cc.o.provides.build: opencog/persist/file/CMakeFiles/savable.dir/TimeServerSavable.cc.o

opencog/persist/file/CMakeFiles/savable.dir/TemporalTableFile.cc.o: opencog/persist/file/CMakeFiles/savable.dir/flags.make
opencog/persist/file/CMakeFiles/savable.dir/TemporalTableFile.cc.o: ../opencog/persist/file/TemporalTableFile.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_6)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/persist/file/CMakeFiles/savable.dir/TemporalTableFile.cc.o"
	cd /opt/opencog/bin/opencog/persist/file && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/savable.dir/TemporalTableFile.cc.o -c /opt/opencog/opencog/persist/file/TemporalTableFile.cc

opencog/persist/file/CMakeFiles/savable.dir/TemporalTableFile.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/savable.dir/TemporalTableFile.cc.i"
	cd /opt/opencog/bin/opencog/persist/file && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/persist/file/TemporalTableFile.cc > CMakeFiles/savable.dir/TemporalTableFile.cc.i

opencog/persist/file/CMakeFiles/savable.dir/TemporalTableFile.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/savable.dir/TemporalTableFile.cc.s"
	cd /opt/opencog/bin/opencog/persist/file && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/persist/file/TemporalTableFile.cc -o CMakeFiles/savable.dir/TemporalTableFile.cc.s

opencog/persist/file/CMakeFiles/savable.dir/TemporalTableFile.cc.o.requires:
.PHONY : opencog/persist/file/CMakeFiles/savable.dir/TemporalTableFile.cc.o.requires

opencog/persist/file/CMakeFiles/savable.dir/TemporalTableFile.cc.o.provides: opencog/persist/file/CMakeFiles/savable.dir/TemporalTableFile.cc.o.requires
	$(MAKE) -f opencog/persist/file/CMakeFiles/savable.dir/build.make opencog/persist/file/CMakeFiles/savable.dir/TemporalTableFile.cc.o.provides.build
.PHONY : opencog/persist/file/CMakeFiles/savable.dir/TemporalTableFile.cc.o.provides

opencog/persist/file/CMakeFiles/savable.dir/TemporalTableFile.cc.o.provides.build: opencog/persist/file/CMakeFiles/savable.dir/TemporalTableFile.cc.o

# Object files for target savable
savable_OBJECTS = \
"CMakeFiles/savable.dir/CoreUtils.cc.o" \
"CMakeFiles/savable.dir/CompositeRenumber.cc.o" \
"CMakeFiles/savable.dir/SavingLoading.cc.o" \
"CMakeFiles/savable.dir/SpaceServerSavable.cc.o" \
"CMakeFiles/savable.dir/TimeServerSavable.cc.o" \
"CMakeFiles/savable.dir/TemporalTableFile.cc.o"

# External object files for target savable
savable_EXTERNAL_OBJECTS =

opencog/persist/file/libsavable.so: opencog/persist/file/CMakeFiles/savable.dir/CoreUtils.cc.o
opencog/persist/file/libsavable.so: opencog/persist/file/CMakeFiles/savable.dir/CompositeRenumber.cc.o
opencog/persist/file/libsavable.so: opencog/persist/file/CMakeFiles/savable.dir/SavingLoading.cc.o
opencog/persist/file/libsavable.so: opencog/persist/file/CMakeFiles/savable.dir/SpaceServerSavable.cc.o
opencog/persist/file/libsavable.so: opencog/persist/file/CMakeFiles/savable.dir/TimeServerSavable.cc.o
opencog/persist/file/libsavable.so: opencog/persist/file/CMakeFiles/savable.dir/TemporalTableFile.cc.o
opencog/persist/file/libsavable.so: opencog/persist/file/CMakeFiles/savable.dir/build.make
opencog/persist/file/libsavable.so: opencog/persist/file/CMakeFiles/savable.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX shared library libsavable.so"
	cd /opt/opencog/bin/opencog/persist/file && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/savable.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
opencog/persist/file/CMakeFiles/savable.dir/build: opencog/persist/file/libsavable.so
.PHONY : opencog/persist/file/CMakeFiles/savable.dir/build

opencog/persist/file/CMakeFiles/savable.dir/requires: opencog/persist/file/CMakeFiles/savable.dir/CoreUtils.cc.o.requires
opencog/persist/file/CMakeFiles/savable.dir/requires: opencog/persist/file/CMakeFiles/savable.dir/CompositeRenumber.cc.o.requires
opencog/persist/file/CMakeFiles/savable.dir/requires: opencog/persist/file/CMakeFiles/savable.dir/SavingLoading.cc.o.requires
opencog/persist/file/CMakeFiles/savable.dir/requires: opencog/persist/file/CMakeFiles/savable.dir/SpaceServerSavable.cc.o.requires
opencog/persist/file/CMakeFiles/savable.dir/requires: opencog/persist/file/CMakeFiles/savable.dir/TimeServerSavable.cc.o.requires
opencog/persist/file/CMakeFiles/savable.dir/requires: opencog/persist/file/CMakeFiles/savable.dir/TemporalTableFile.cc.o.requires
.PHONY : opencog/persist/file/CMakeFiles/savable.dir/requires

opencog/persist/file/CMakeFiles/savable.dir/clean:
	cd /opt/opencog/bin/opencog/persist/file && $(CMAKE_COMMAND) -P CMakeFiles/savable.dir/cmake_clean.cmake
.PHONY : opencog/persist/file/CMakeFiles/savable.dir/clean

opencog/persist/file/CMakeFiles/savable.dir/depend:
	cd /opt/opencog/bin && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /opt/opencog /opt/opencog/opencog/persist/file /opt/opencog/bin /opt/opencog/bin/opencog/persist/file /opt/opencog/bin/opencog/persist/file/CMakeFiles/savable.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : opencog/persist/file/CMakeFiles/savable.dir/depend

