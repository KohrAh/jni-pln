# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/persist/file/CompositeRenumber.cc" "/opt/opencog/bin/opencog/persist/file/CMakeFiles/savable.dir/CompositeRenumber.cc.o"
  "/opt/opencog/opencog/persist/file/CoreUtils.cc" "/opt/opencog/bin/opencog/persist/file/CMakeFiles/savable.dir/CoreUtils.cc.o"
  "/opt/opencog/opencog/persist/file/SavingLoading.cc" "/opt/opencog/bin/opencog/persist/file/CMakeFiles/savable.dir/SavingLoading.cc.o"
  "/opt/opencog/opencog/persist/file/SpaceServerSavable.cc" "/opt/opencog/bin/opencog/persist/file/CMakeFiles/savable.dir/SpaceServerSavable.cc.o"
  "/opt/opencog/opencog/persist/file/TemporalTableFile.cc" "/opt/opencog/bin/opencog/persist/file/CMakeFiles/savable.dir/TemporalTableFile.cc.o"
  "/opt/opencog/opencog/persist/file/TimeServerSavable.cc" "/opt/opencog/bin/opencog/persist/file/CMakeFiles/savable.dir/TimeServerSavable.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )
