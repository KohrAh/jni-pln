# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/persist/xml/FileXMLBufferReader.cc" "/opt/opencog/bin/opencog/persist/xml/CMakeFiles/xml.dir/FileXMLBufferReader.cc.o"
  "/opt/opencog/opencog/persist/xml/NMXmlExporter.cc" "/opt/opencog/bin/opencog/persist/xml/CMakeFiles/xml.dir/NMXmlExporter.cc.o"
  "/opt/opencog/opencog/persist/xml/NMXmlParser.cc" "/opt/opencog/bin/opencog/persist/xml/CMakeFiles/xml.dir/NMXmlParser.cc.o"
  "/opt/opencog/opencog/persist/xml/StringXMLBufferReader.cc" "/opt/opencog/bin/opencog/persist/xml/CMakeFiles/xml.dir/StringXMLBufferReader.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )
