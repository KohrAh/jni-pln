# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canoncical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /opt/opencog

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /opt/opencog/bin

# Include any dependencies generated for this target.
include opencog/persist/xml/CMakeFiles/xml.dir/depend.make

# Include the progress variables for this target.
include opencog/persist/xml/CMakeFiles/xml.dir/progress.make

# Include the compile flags for this target's objects.
include opencog/persist/xml/CMakeFiles/xml.dir/flags.make

opencog/persist/xml/CMakeFiles/xml.dir/FileXMLBufferReader.cc.o: opencog/persist/xml/CMakeFiles/xml.dir/flags.make
opencog/persist/xml/CMakeFiles/xml.dir/FileXMLBufferReader.cc.o: ../opencog/persist/xml/FileXMLBufferReader.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/persist/xml/CMakeFiles/xml.dir/FileXMLBufferReader.cc.o"
	cd /opt/opencog/bin/opencog/persist/xml && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/xml.dir/FileXMLBufferReader.cc.o -c /opt/opencog/opencog/persist/xml/FileXMLBufferReader.cc

opencog/persist/xml/CMakeFiles/xml.dir/FileXMLBufferReader.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/xml.dir/FileXMLBufferReader.cc.i"
	cd /opt/opencog/bin/opencog/persist/xml && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/persist/xml/FileXMLBufferReader.cc > CMakeFiles/xml.dir/FileXMLBufferReader.cc.i

opencog/persist/xml/CMakeFiles/xml.dir/FileXMLBufferReader.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/xml.dir/FileXMLBufferReader.cc.s"
	cd /opt/opencog/bin/opencog/persist/xml && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/persist/xml/FileXMLBufferReader.cc -o CMakeFiles/xml.dir/FileXMLBufferReader.cc.s

opencog/persist/xml/CMakeFiles/xml.dir/FileXMLBufferReader.cc.o.requires:
.PHONY : opencog/persist/xml/CMakeFiles/xml.dir/FileXMLBufferReader.cc.o.requires

opencog/persist/xml/CMakeFiles/xml.dir/FileXMLBufferReader.cc.o.provides: opencog/persist/xml/CMakeFiles/xml.dir/FileXMLBufferReader.cc.o.requires
	$(MAKE) -f opencog/persist/xml/CMakeFiles/xml.dir/build.make opencog/persist/xml/CMakeFiles/xml.dir/FileXMLBufferReader.cc.o.provides.build
.PHONY : opencog/persist/xml/CMakeFiles/xml.dir/FileXMLBufferReader.cc.o.provides

opencog/persist/xml/CMakeFiles/xml.dir/FileXMLBufferReader.cc.o.provides.build: opencog/persist/xml/CMakeFiles/xml.dir/FileXMLBufferReader.cc.o

opencog/persist/xml/CMakeFiles/xml.dir/NMXmlExporter.cc.o: opencog/persist/xml/CMakeFiles/xml.dir/flags.make
opencog/persist/xml/CMakeFiles/xml.dir/NMXmlExporter.cc.o: ../opencog/persist/xml/NMXmlExporter.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/persist/xml/CMakeFiles/xml.dir/NMXmlExporter.cc.o"
	cd /opt/opencog/bin/opencog/persist/xml && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/xml.dir/NMXmlExporter.cc.o -c /opt/opencog/opencog/persist/xml/NMXmlExporter.cc

opencog/persist/xml/CMakeFiles/xml.dir/NMXmlExporter.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/xml.dir/NMXmlExporter.cc.i"
	cd /opt/opencog/bin/opencog/persist/xml && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/persist/xml/NMXmlExporter.cc > CMakeFiles/xml.dir/NMXmlExporter.cc.i

opencog/persist/xml/CMakeFiles/xml.dir/NMXmlExporter.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/xml.dir/NMXmlExporter.cc.s"
	cd /opt/opencog/bin/opencog/persist/xml && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/persist/xml/NMXmlExporter.cc -o CMakeFiles/xml.dir/NMXmlExporter.cc.s

opencog/persist/xml/CMakeFiles/xml.dir/NMXmlExporter.cc.o.requires:
.PHONY : opencog/persist/xml/CMakeFiles/xml.dir/NMXmlExporter.cc.o.requires

opencog/persist/xml/CMakeFiles/xml.dir/NMXmlExporter.cc.o.provides: opencog/persist/xml/CMakeFiles/xml.dir/NMXmlExporter.cc.o.requires
	$(MAKE) -f opencog/persist/xml/CMakeFiles/xml.dir/build.make opencog/persist/xml/CMakeFiles/xml.dir/NMXmlExporter.cc.o.provides.build
.PHONY : opencog/persist/xml/CMakeFiles/xml.dir/NMXmlExporter.cc.o.provides

opencog/persist/xml/CMakeFiles/xml.dir/NMXmlExporter.cc.o.provides.build: opencog/persist/xml/CMakeFiles/xml.dir/NMXmlExporter.cc.o

opencog/persist/xml/CMakeFiles/xml.dir/NMXmlParser.cc.o: opencog/persist/xml/CMakeFiles/xml.dir/flags.make
opencog/persist/xml/CMakeFiles/xml.dir/NMXmlParser.cc.o: ../opencog/persist/xml/NMXmlParser.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_3)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/persist/xml/CMakeFiles/xml.dir/NMXmlParser.cc.o"
	cd /opt/opencog/bin/opencog/persist/xml && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/xml.dir/NMXmlParser.cc.o -c /opt/opencog/opencog/persist/xml/NMXmlParser.cc

opencog/persist/xml/CMakeFiles/xml.dir/NMXmlParser.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/xml.dir/NMXmlParser.cc.i"
	cd /opt/opencog/bin/opencog/persist/xml && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/persist/xml/NMXmlParser.cc > CMakeFiles/xml.dir/NMXmlParser.cc.i

opencog/persist/xml/CMakeFiles/xml.dir/NMXmlParser.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/xml.dir/NMXmlParser.cc.s"
	cd /opt/opencog/bin/opencog/persist/xml && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/persist/xml/NMXmlParser.cc -o CMakeFiles/xml.dir/NMXmlParser.cc.s

opencog/persist/xml/CMakeFiles/xml.dir/NMXmlParser.cc.o.requires:
.PHONY : opencog/persist/xml/CMakeFiles/xml.dir/NMXmlParser.cc.o.requires

opencog/persist/xml/CMakeFiles/xml.dir/NMXmlParser.cc.o.provides: opencog/persist/xml/CMakeFiles/xml.dir/NMXmlParser.cc.o.requires
	$(MAKE) -f opencog/persist/xml/CMakeFiles/xml.dir/build.make opencog/persist/xml/CMakeFiles/xml.dir/NMXmlParser.cc.o.provides.build
.PHONY : opencog/persist/xml/CMakeFiles/xml.dir/NMXmlParser.cc.o.provides

opencog/persist/xml/CMakeFiles/xml.dir/NMXmlParser.cc.o.provides.build: opencog/persist/xml/CMakeFiles/xml.dir/NMXmlParser.cc.o

opencog/persist/xml/CMakeFiles/xml.dir/StringXMLBufferReader.cc.o: opencog/persist/xml/CMakeFiles/xml.dir/flags.make
opencog/persist/xml/CMakeFiles/xml.dir/StringXMLBufferReader.cc.o: ../opencog/persist/xml/StringXMLBufferReader.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /opt/opencog/bin/CMakeFiles $(CMAKE_PROGRESS_4)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object opencog/persist/xml/CMakeFiles/xml.dir/StringXMLBufferReader.cc.o"
	cd /opt/opencog/bin/opencog/persist/xml && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/xml.dir/StringXMLBufferReader.cc.o -c /opt/opencog/opencog/persist/xml/StringXMLBufferReader.cc

opencog/persist/xml/CMakeFiles/xml.dir/StringXMLBufferReader.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/xml.dir/StringXMLBufferReader.cc.i"
	cd /opt/opencog/bin/opencog/persist/xml && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /opt/opencog/opencog/persist/xml/StringXMLBufferReader.cc > CMakeFiles/xml.dir/StringXMLBufferReader.cc.i

opencog/persist/xml/CMakeFiles/xml.dir/StringXMLBufferReader.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/xml.dir/StringXMLBufferReader.cc.s"
	cd /opt/opencog/bin/opencog/persist/xml && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /opt/opencog/opencog/persist/xml/StringXMLBufferReader.cc -o CMakeFiles/xml.dir/StringXMLBufferReader.cc.s

opencog/persist/xml/CMakeFiles/xml.dir/StringXMLBufferReader.cc.o.requires:
.PHONY : opencog/persist/xml/CMakeFiles/xml.dir/StringXMLBufferReader.cc.o.requires

opencog/persist/xml/CMakeFiles/xml.dir/StringXMLBufferReader.cc.o.provides: opencog/persist/xml/CMakeFiles/xml.dir/StringXMLBufferReader.cc.o.requires
	$(MAKE) -f opencog/persist/xml/CMakeFiles/xml.dir/build.make opencog/persist/xml/CMakeFiles/xml.dir/StringXMLBufferReader.cc.o.provides.build
.PHONY : opencog/persist/xml/CMakeFiles/xml.dir/StringXMLBufferReader.cc.o.provides

opencog/persist/xml/CMakeFiles/xml.dir/StringXMLBufferReader.cc.o.provides.build: opencog/persist/xml/CMakeFiles/xml.dir/StringXMLBufferReader.cc.o

# Object files for target xml
xml_OBJECTS = \
"CMakeFiles/xml.dir/FileXMLBufferReader.cc.o" \
"CMakeFiles/xml.dir/NMXmlExporter.cc.o" \
"CMakeFiles/xml.dir/NMXmlParser.cc.o" \
"CMakeFiles/xml.dir/StringXMLBufferReader.cc.o"

# External object files for target xml
xml_EXTERNAL_OBJECTS =

opencog/persist/xml/libxml.so: opencog/persist/xml/CMakeFiles/xml.dir/FileXMLBufferReader.cc.o
opencog/persist/xml/libxml.so: opencog/persist/xml/CMakeFiles/xml.dir/NMXmlExporter.cc.o
opencog/persist/xml/libxml.so: opencog/persist/xml/CMakeFiles/xml.dir/NMXmlParser.cc.o
opencog/persist/xml/libxml.so: opencog/persist/xml/CMakeFiles/xml.dir/StringXMLBufferReader.cc.o
opencog/persist/xml/libxml.so: /usr/lib/i386-linux-gnu/libexpat.so
opencog/persist/xml/libxml.so: /usr/lib/i386-linux-gnu/libexpat.so
opencog/persist/xml/libxml.so: opencog/persist/xml/CMakeFiles/xml.dir/build.make
opencog/persist/xml/libxml.so: opencog/persist/xml/CMakeFiles/xml.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX shared library libxml.so"
	cd /opt/opencog/bin/opencog/persist/xml && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/xml.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
opencog/persist/xml/CMakeFiles/xml.dir/build: opencog/persist/xml/libxml.so
.PHONY : opencog/persist/xml/CMakeFiles/xml.dir/build

opencog/persist/xml/CMakeFiles/xml.dir/requires: opencog/persist/xml/CMakeFiles/xml.dir/FileXMLBufferReader.cc.o.requires
opencog/persist/xml/CMakeFiles/xml.dir/requires: opencog/persist/xml/CMakeFiles/xml.dir/NMXmlExporter.cc.o.requires
opencog/persist/xml/CMakeFiles/xml.dir/requires: opencog/persist/xml/CMakeFiles/xml.dir/NMXmlParser.cc.o.requires
opencog/persist/xml/CMakeFiles/xml.dir/requires: opencog/persist/xml/CMakeFiles/xml.dir/StringXMLBufferReader.cc.o.requires
.PHONY : opencog/persist/xml/CMakeFiles/xml.dir/requires

opencog/persist/xml/CMakeFiles/xml.dir/clean:
	cd /opt/opencog/bin/opencog/persist/xml && $(CMAKE_COMMAND) -P CMakeFiles/xml.dir/cmake_clean.cmake
.PHONY : opencog/persist/xml/CMakeFiles/xml.dir/clean

opencog/persist/xml/CMakeFiles/xml.dir/depend:
	cd /opt/opencog/bin && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /opt/opencog /opt/opencog/opencog/persist/xml /opt/opencog/bin /opt/opencog/bin/opencog/persist/xml /opt/opencog/bin/opencog/persist/xml/CMakeFiles/xml.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : opencog/persist/xml/CMakeFiles/xml.dir/depend

