# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/nlp/chatbot/CogitaConfig.cc" "/opt/opencog/bin/opencog/nlp/chatbot/CMakeFiles/cogita.dir/CogitaConfig.cc.o"
  "/opt/opencog/opencog/nlp/chatbot/IRC.cc" "/opt/opencog/bin/opencog/nlp/chatbot/CMakeFiles/cogita.dir/IRC.cc.o"
  "/opt/opencog/opencog/nlp/chatbot/go-irc.cc" "/opt/opencog/bin/opencog/nlp/chatbot/CMakeFiles/cogita.dir/go-irc.cc.o"
  "/opt/opencog/opencog/nlp/chatbot/whirr-sockets.cc" "/opt/opencog/bin/opencog/nlp/chatbot/CMakeFiles/cogita.dir/whirr-sockets.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/DependInfo.cmake"
  )
