# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/nlp/wsd/EdgeThin.cc" "/opt/opencog/bin/opencog/nlp/wsd/CMakeFiles/wsd.dir/EdgeThin.cc.o"
  "/opt/opencog/opencog/nlp/wsd/EdgeUtils.cc" "/opt/opencog/bin/opencog/nlp/wsd/CMakeFiles/wsd.dir/EdgeUtils.cc.o"
  "/opt/opencog/opencog/nlp/wsd/Mihalcea.cc" "/opt/opencog/bin/opencog/nlp/wsd/CMakeFiles/wsd.dir/Mihalcea.cc.o"
  "/opt/opencog/opencog/nlp/wsd/MihalceaEdge.cc" "/opt/opencog/bin/opencog/nlp/wsd/CMakeFiles/wsd.dir/MihalceaEdge.cc.o"
  "/opt/opencog/opencog/nlp/wsd/MihalceaLabel.cc" "/opt/opencog/bin/opencog/nlp/wsd/CMakeFiles/wsd.dir/MihalceaLabel.cc.o"
  "/opt/opencog/opencog/nlp/wsd/NNAdjust.cc" "/opt/opencog/bin/opencog/nlp/wsd/CMakeFiles/wsd.dir/NNAdjust.cc.o"
  "/opt/opencog/opencog/nlp/wsd/ParseRank.cc" "/opt/opencog/bin/opencog/nlp/wsd/CMakeFiles/wsd.dir/ParseRank.cc.o"
  "/opt/opencog/opencog/nlp/wsd/ReportRank.cc" "/opt/opencog/bin/opencog/nlp/wsd/CMakeFiles/wsd.dir/ReportRank.cc.o"
  "/opt/opencog/opencog/nlp/wsd/SenseCache.cc" "/opt/opencog/bin/opencog/nlp/wsd/CMakeFiles/wsd.dir/SenseCache.cc.o"
  "/opt/opencog/opencog/nlp/wsd/SenseRank.cc" "/opt/opencog/bin/opencog/nlp/wsd/CMakeFiles/wsd.dir/SenseRank.cc.o"
  "/opt/opencog/opencog/nlp/wsd/SenseSimilarityLCH.cc" "/opt/opencog/bin/opencog/nlp/wsd/CMakeFiles/wsd.dir/SenseSimilarityLCH.cc.o"
  "/opt/opencog/opencog/nlp/wsd/SenseSimilaritySQL.cc" "/opt/opencog/bin/opencog/nlp/wsd/CMakeFiles/wsd.dir/SenseSimilaritySQL.cc.o"
  "/opt/opencog/opencog/nlp/wsd/Sweep.cc" "/opt/opencog/bin/opencog/nlp/wsd/CMakeFiles/wsd.dir/Sweep.cc.o"
  "/opt/opencog/opencog/nlp/wsd/WordSenseProcessor.cc" "/opt/opencog/bin/opencog/nlp/wsd/CMakeFiles/wsd.dir/WordSenseProcessor.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opt/opencog/bin/opencog/persist/sql/CMakeFiles/persist.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/guile/CMakeFiles/smob.dir/DependInfo.cmake"
  )
