# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/comboreduct/ant_combo_vocabulary/ant_action_symbol.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/ant_combo_vocabulary/ant_action_symbol.cc.o"
  "/opt/opencog/opencog/comboreduct/ant_combo_vocabulary/ant_builtin_action.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/ant_combo_vocabulary/ant_builtin_action.cc.o"
  "/opt/opencog/opencog/comboreduct/ant_combo_vocabulary/ant_combo_vocabulary.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/ant_combo_vocabulary/ant_combo_vocabulary.cc.o"
  "/opt/opencog/opencog/comboreduct/ant_combo_vocabulary/ant_indefinite_object.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/ant_combo_vocabulary/ant_indefinite_object.cc.o"
  "/opt/opencog/opencog/comboreduct/ant_combo_vocabulary/ant_perception.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/ant_combo_vocabulary/ant_perception.cc.o"
  "/opt/opencog/opencog/comboreduct/combo/action.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/combo/action.cc.o"
  "/opt/opencog/opencog/comboreduct/combo/action_symbol.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/combo/action_symbol.cc.o"
  "/opt/opencog/opencog/comboreduct/combo/assumption.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/combo/assumption.cc.o"
  "/opt/opencog/opencog/comboreduct/combo/builtin_action.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/combo/builtin_action.cc.o"
  "/opt/opencog/opencog/comboreduct/combo/definite_object.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/combo/definite_object.cc.o"
  "/opt/opencog/opencog/comboreduct/combo/eval.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/combo/eval.cc.o"
  "/opt/opencog/opencog/comboreduct/combo/indefinite_object.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/combo/indefinite_object.cc.o"
  "/opt/opencog/opencog/comboreduct/combo/message.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/combo/message.cc.o"
  "/opt/opencog/opencog/comboreduct/combo/perception.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/combo/perception.cc.o"
  "/opt/opencog/opencog/comboreduct/combo/procedure_call.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/combo/procedure_call.cc.o"
  "/opt/opencog/opencog/comboreduct/combo/procedure_repository.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/combo/procedure_repository.cc.o"
  "/opt/opencog/opencog/comboreduct/combo/simple_nn.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/combo/simple_nn.cc.o"
  "/opt/opencog/opencog/comboreduct/combo/table.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/combo/table.cc.o"
  "/opt/opencog/opencog/comboreduct/combo/type_tree.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/combo/type_tree.cc.o"
  "/opt/opencog/opencog/comboreduct/combo/type_tree_def.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/combo/type_tree_def.cc.o"
  "/opt/opencog/opencog/comboreduct/combo/variable_unifier.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/combo/variable_unifier.cc.o"
  "/opt/opencog/opencog/comboreduct/combo/vertex.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/combo/vertex.cc.o"
  "/opt/opencog/opencog/comboreduct/crutil/exception.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/crutil/exception.cc.o"
  "/opt/opencog/opencog/comboreduct/reduct/action_reduction.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/reduct/action_reduction.cc.o"
  "/opt/opencog/opencog/comboreduct/reduct/action_rules.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/reduct/action_rules.cc.o"
  "/opt/opencog/opencog/comboreduct/reduct/ann_reduction.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/reduct/ann_reduction.cc.o"
  "/opt/opencog/opencog/comboreduct/reduct/clean_reduction.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/reduct/clean_reduction.cc.o"
  "/opt/opencog/opencog/comboreduct/reduct/contin_reduction.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/reduct/contin_reduction.cc.o"
  "/opt/opencog/opencog/comboreduct/reduct/contin_rules.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/reduct/contin_rules.cc.o"
  "/opt/opencog/opencog/comboreduct/reduct/flat_normal_form.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/reduct/flat_normal_form.cc.o"
  "/opt/opencog/opencog/comboreduct/reduct/full_reduction.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/reduct/full_reduction.cc.o"
  "/opt/opencog/opencog/comboreduct/reduct/general_rules.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/reduct/general_rules.cc.o"
  "/opt/opencog/opencog/comboreduct/reduct/logical_reduction.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/reduct/logical_reduction.cc.o"
  "/opt/opencog/opencog/comboreduct/reduct/logical_rules.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/reduct/logical_rules.cc.o"
  "/opt/opencog/opencog/comboreduct/reduct/meta_rules.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/reduct/meta_rules.cc.o"
  "/opt/opencog/opencog/comboreduct/reduct/mixed_reduction.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/reduct/mixed_reduction.cc.o"
  "/opt/opencog/opencog/comboreduct/reduct/mixed_rules.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/reduct/mixed_rules.cc.o"
  "/opt/opencog/opencog/comboreduct/reduct/perception_reduction.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/reduct/perception_reduction.cc.o"
  "/opt/opencog/opencog/comboreduct/reduct/perception_rules.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/reduct/perception_rules.cc.o"
  "/opt/opencog/opencog/comboreduct/reduct/reduct.cc" "/opt/opencog/bin/opencog/comboreduct/CMakeFiles/comboreduct.dir/reduct/reduct.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/DependInfo.cmake"
  )
