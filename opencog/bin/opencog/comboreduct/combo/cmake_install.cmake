# Install script for directory: /opt/opencog/opencog/comboreduct/combo

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Debug")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/opencog/combo" TYPE FILE FILES
    "/opt/opencog/opencog/comboreduct/combo/action.h"
    "/opt/opencog/opencog/comboreduct/combo/action_eval.h"
    "/opt/opencog/opencog/comboreduct/combo/action_symbol.h"
    "/opt/opencog/opencog/comboreduct/combo/builtin_action.h"
    "/opt/opencog/opencog/comboreduct/combo/message.h"
    "/opt/opencog/opencog/comboreduct/combo/action_symbol.h"
    "/opt/opencog/opencog/comboreduct/combo/indefinite_object.h"
    "/opt/opencog/opencog/comboreduct/combo/definite_object.h"
    "/opt/opencog/opencog/comboreduct/combo/procedure_call.h"
    "/opt/opencog/opencog/comboreduct/combo/procedure_repository.h"
    "/opt/opencog/opencog/comboreduct/combo/assumption.h"
    "/opt/opencog/opencog/comboreduct/combo/eval.h"
    "/opt/opencog/opencog/comboreduct/combo/table.h"
    "/opt/opencog/opencog/comboreduct/combo/perception.h"
    "/opt/opencog/opencog/comboreduct/combo/perception_eval.h"
    "/opt/opencog/opencog/comboreduct/combo/tree_generation.h"
    "/opt/opencog/opencog/comboreduct/combo/type_tree_def.h"
    "/opt/opencog/opencog/comboreduct/combo/type_tree.h"
    "/opt/opencog/opencog/comboreduct/combo/vertex.h"
    "/opt/opencog/opencog/comboreduct/combo/using.h"
    "/opt/opencog/opencog/comboreduct/combo/variable_unifier.h"
    "/opt/opencog/opencog/comboreduct/combo/operator_base.h"
    "/opt/opencog/opencog/comboreduct/combo/common_def.h"
    "/opt/opencog/opencog/comboreduct/combo/simple_nn.h"
    "/opt/opencog/opencog/comboreduct/combo/convert_ann_combo.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

