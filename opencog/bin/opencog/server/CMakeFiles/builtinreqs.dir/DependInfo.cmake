# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/server/BuiltinRequestsModule.cc" "/opt/opencog/bin/opencog/server/CMakeFiles/builtinreqs.dir/BuiltinRequestsModule.cc.o"
  "/opt/opencog/opencog/server/DataRequest.cc" "/opt/opencog/bin/opencog/server/CMakeFiles/builtinreqs.dir/DataRequest.cc.o"
  "/opt/opencog/opencog/server/ExitRequest.cc" "/opt/opencog/bin/opencog/server/CMakeFiles/builtinreqs.dir/ExitRequest.cc.o"
  "/opt/opencog/opencog/server/HelpRequest.cc" "/opt/opencog/bin/opencog/server/CMakeFiles/builtinreqs.dir/HelpRequest.cc.o"
  "/opt/opencog/opencog/server/ListRequest.cc" "/opt/opencog/bin/opencog/server/CMakeFiles/builtinreqs.dir/ListRequest.cc.o"
  "/opt/opencog/opencog/server/LoadModuleRequest.cc" "/opt/opencog/bin/opencog/server/CMakeFiles/builtinreqs.dir/LoadModuleRequest.cc.o"
  "/opt/opencog/opencog/server/LoadRequest.cc" "/opt/opencog/bin/opencog/server/CMakeFiles/builtinreqs.dir/LoadRequest.cc.o"
  "/opt/opencog/opencog/server/Request.cc" "/opt/opencog/bin/opencog/server/CMakeFiles/builtinreqs.dir/Request.cc.o"
  "/opt/opencog/opencog/server/SaveRequest.cc" "/opt/opencog/bin/opencog/server/CMakeFiles/builtinreqs.dir/SaveRequest.cc.o"
  "/opt/opencog/opencog/server/ShutdownRequest.cc" "/opt/opencog/bin/opencog/server/CMakeFiles/builtinreqs.dir/ShutdownRequest.cc.o"
  "/opt/opencog/opencog/server/SleepRequest.cc" "/opt/opencog/bin/opencog/server/CMakeFiles/builtinreqs.dir/SleepRequest.cc.o"
  "/opt/opencog/opencog/server/UnloadModuleRequest.cc" "/opt/opencog/bin/opencog/server/CMakeFiles/builtinreqs.dir/UnloadModuleRequest.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/persist/xml/CMakeFiles/xml.dir/DependInfo.cmake"
  )
