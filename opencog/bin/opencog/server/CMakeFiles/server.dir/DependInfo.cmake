# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/opt/opencog/opencog/server/Agent.cc" "/opt/opencog/bin/opencog/server/CMakeFiles/server.dir/Agent.cc.o"
  "/opt/opencog/opencog/server/BaseServer.cc" "/opt/opencog/bin/opencog/server/CMakeFiles/server.dir/BaseServer.cc.o"
  "/opt/opencog/opencog/server/CogServer.cc" "/opt/opencog/bin/opencog/server/CMakeFiles/server.dir/CogServer.cc.o"
  "/opt/opencog/opencog/server/ConsoleSocket.cc" "/opt/opencog/bin/opencog/server/CMakeFiles/server.dir/ConsoleSocket.cc.o"
  "/opt/opencog/opencog/server/NetworkServer.cc" "/opt/opencog/bin/opencog/server/CMakeFiles/server.dir/NetworkServer.cc.o"
  "/opt/opencog/opencog/server/Request.cc" "/opt/opencog/bin/opencog/server/CMakeFiles/server.dir/Request.cc.o"
  "/opt/opencog/opencog/server/ServerSocket.cc" "/opt/opencog/bin/opencog/server/CMakeFiles/server.dir/ServerSocket.cc.o"
  "/opt/opencog/opencog/server/SystemActivityTable.cc" "/opt/opencog/bin/opencog/server/CMakeFiles/server.dir/SystemActivityTable.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EXPAT"
  "HAVE_GSL"
  "HAVE_GUILE"
  "HAVE_PROTOBUF"
  "HAVE_CYTHON"
  "HAVE_SQL_STORAGE"
  "HAVE_UBIGRAPH"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/opt/opencog/bin/opencog/persist/sql/CMakeFiles/persist.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/guile/CMakeFiles/smob.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/persist/xml/CMakeFiles/xml.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/atomspace/CMakeFiles/atomspace.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/util/CMakeFiles/util.dir/DependInfo.cmake"
  "/opt/opencog/bin/opencog/spatial/CMakeFiles/SpaceMap.dir/DependInfo.cmake"
  )
