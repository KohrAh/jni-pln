.\"                                      Hey, EMACS: -*- nroff -*-
.\" Man page for moses-exec
.\"
.\" Copyright (C) 2011,2012 Linas Vepstas
.\"
.\" First parameter, NAME, should be all caps
.\" Second parameter, SECTION, should be 1-8, maybe w/ subsection
.\" other parameters are allowed: see man(7), man(1)
.pc
.TH MOSES 1 "January 23, 2012"
.LO 1
.\" Please adjust this date whenever revising the manpage.
.\"
.\" Some roff macros, for reference:
.\" .nh        disable hyphenation
.\" .hy        enable hyphenation
.\" .ad l      left justify
.\" .ad b      justify to both left and right margins
.\" .nf        disable filling
.\" .fi        enable filling
.\" .br        insert line break
.\" .sp <n>    insert n+1 empty lines
.\" for manpage-specific macros, see man(7)
.SH NAME
moses \- meta-optimizing semantic evolutionary search solver
.SH SYNOPSIS
.\" The help & version command line
.B moses
.RB \-h
.br
.\" The general command line
.B moses
.RB [ \-A
.IR max_score ]
.RB [ \-a
.IR algo ]
.RB [ \-B
.IR knob_effort ]
.RB [ \-b
.IR nsamples ]
.RB [ \-C1 ]
.RB [ \-c
.IR result_count ]
.RB [ \-D
.IR max_dist ]
.RB [ \-d1 ]
.RB [ \-E
.IR reduct_effort ]
.RB [ \-e
.IR exemplar ]
.RB [ \-F ]
.RB [ \-f
.IR logfile ]
.RB [ \-g
.IR max_gens ]
.RB [ \-I0 ]
.RB [ \-i
.IR filename ]
.RB [ \-j
.IR jobs ]
.RB [ \-k
.IR problem_size ]
.RB [ \-L1 ]
.RB [ \-l
.IR loglevel ]
.RB [ \-m
.IR max_evals ]
.RB [ \-N
.IR oper ]
.RB [ \-n
.IR oper ]
.RB [ \-o
.IR output_file ]
.RB [ \-P
.IR pop_size_ratio ]
.RB [ \-q
.IR min ]
.RB [ \-R
.IR threshold ]
.RB [ \-r
.IR random_seed ]
.RB [ \-S0 ]
.RB [ \-s1 ]
.RB [ \-t1 ]
.RB [ \-T1 ]
.RB [ \-u
.IR target_feature ]
.RB [ \-V1 ]
.RB [ \-v
.IR temperature ]
.RB [ \-W1 ]
.RB [ \-w
.IR max ]
.RB [ \-x1 ]
.RB [ \-y
.IR combo_program ]
.RB [ \-z
.IR complexity_ratio ]
.SH DESCRIPTION
.PP
.\" TeX users may be more comfortable with the \fB<whatever>\fP and
.\" \fI<whatever>\fP escape sequences to invode bold face and italics,
.\" respectively.
\fBmoses\fP is the command-line wrapper to the MOSES program learning
library. It may be used to solve learning tasks specified with file
inputs, or to run various demonstration problems.  Given an input table
of values, it will perform a regression, generating, as output,
a \fBcombo\fP program that best fits the data. \fBmoses\fP is
multi-threaded, and can be distrubited across multiple machines to
improve run-time performance.
.PP
.\" ============================================================
.SH EXAMPLE
As an example, the input file of comma-separated values:

.nf
\& a,b,out
\& 0,0,0
\& 0,1,0
\& 1,0,0
\& 1,1,1
.fi

when run with the flags \fI\-u\ out\ \-W1\fR will generate the combo
program \fIand($a\ $b)\fR. This program indicates that the third column
can be modelled as the boolean-and of the first two.  The \fI\-u\fR option
specifies that it is the third column that should be modelled, and the
\fI\-W1\fR flag indicates that column labels, rather than column numbers,
should be printed on output.

.PP
.\" ============================================================
.SH OVERVIEW
\fBmoses\fP implements program learning by using a meta-optimization
algorithm. That is, it uses two optimization algorithms, one wrapped inside
the other, to find solutions.  The outer optimization algorithm selects
candidate programs (called \fIexemplars\fP), and then creates similar
programs by taking an exemplar and inserting variables (called
\fIknobs\fP) in selected locations. The inner optimization algorithm
then searches for the best possible 'knob settings', returning a set
of programs (a \fIdeme\fP) that provide the best fit to the data. The
outer optimization algorithm then selects new candidates, and performs
a simplification step, reducing these to a normal form, to create new
exemplars.  The process is then repeated until a perfect score is
reached, a maximum number of cycles is performed, or forward progress
is no longer being made.
.PP
Program reduction is performed at two distinct stages: after inserting new
knobs, and when candidates are selected.  Reduction takes a program, and
transforms it into a new form, the so-called \fIelegant normal form\fP,
which, roughly speaking, expresses the program in the smallest, most
compact form possible.  Reduction is an important step of the algorithm,
as it prevents the evolved programs from turning into 'spaghetti code',
avoiding needless complexity that can cost during program evaluation.
.PP
The operation of \fBmoses\fP can be modified by using a large variety of
options to control different parts of the above meta-algorithm.
The inner optimization step can be done using one of several different
algorithms, including \fIhillclimbing\fP, \fIsimulated annealing\fP,
or \fIunivariate Bayesian optimization\fP.  The amount of effort
spent in the inner loop can be limited, as compared to how frequently
new demes and exemplars are choosen. The total number of demes or
exemplars maintained in the population may be controlled.  The effort
expended on program normalization at each of the two steps can be
controlled.
.PP
In addition to controlling the operation of the algorithm, many
options are used to describe the format of the input data, and to
control the printing of the output.
.PP
\fBmoses\fP also has a 'demo' mode: it has a number of built-in
scoring functions for various 'well-known' optimization problems,
such as parity, disjunction, and multipilex. Each of these typically
presents a different set of challenges to the optimization algorithm,
and are used to illustrate 'hard' optimization problems.  These demo
problems are useful both for learning and undertanding the use of
\fBmoses\fP, and also for developers and maintainers of \fBmoses\fP
itself, when testing new features and enhancements.

.PP
.\" ============================================================
.SH COMBO PROGRAMS
This section provides a breif overview of the combo programming
language.  Combo is not meant so much for general use by humans
for practical programming, as it is to fit the needs of program
learning.  As such, it is fairly minimal, while still being expressive
enough so that all common programming constructs are simply represented
in combo.  It is not difficult to convert combo programs into
other languages, or to evaluate them directly.
.PP
In combo, all programs are represented as program trees. That
is, each internal node in the tree is an operation and leaves are
either constants or variables. The set of all of the variables in
a program are taken as the arguments to the program. The program
is evaluated by supplying explicit values for the variables,
and then applying operations until the root node is reached.
.PP
By convention, variables are denoted by a hash-mark followed by
a number, although they may also be named. Variables are not 
explicitly typed; they are only implicitly typed during program
evaluation. Thus, for example, \fIor ($1 $2)\fP is a combo
program that represents the boolean-or of two inputs,
\fI$1\fP and \fI$2\fP.
.PP
Logical operators include \fIand\fR, \fIor\fR and \fInot\fR.
The boolean \fIif\fR operator takes three arguments: if the first
argument evaluates to true, then the \fIif\fR evalues to the second
argument, else it evaluates to the third.
Arithmetic operators include +, - * and / for addition, subtraction,
multiplication and division. Additional arithmetic operators
include \fIsin\fR, \fIlog\fR, \fIexp\fR and \fIrand\fR. The \fIrand\fR
operator returns a value in the range from 0.0 to 1.0.  The predicate 
operator \fI0<\fR (greater than zero) takes a single continuous-valued
expression, and returns a boolean. The operator \fIimpulse\fR does 
the opposite: it takes a single boolean-valued expression, and returns
0.0 or 1.0, depending on whether the argument evaluates to false or true.
The continuous-valued \fIif\fR operator functions analogously to the
boolean \fIif\fR, but takes continuous-valued arguements.
.PP
Thus, as an example, the expression \fIand( 0<( +($x 3))  0<($y))\fR
evaluates to \fItrue\fR if \fI$x\fR is greater than -3 and \fI$y\fR
is greater than zero.
.PP
The above operators are built-in; combo may also be extended with
custom-defined operators, although C++ programming and recompilation
is required for this.
.PP
.\" ============================================================
.SH OPTIONS
.PP
Options fall into four classes: those for specifying inputs,
controlling operation, printing output, and running
\fBmoses\fP in demo mode.

.SS "General options"
.TP
.BI \-e\  exemplar
Specify an initial \fIexemplar\fR to use. Each exemplar is written in
combo. May be specified multiple times.
.TP
.B \-h, --help
Print help.
.TP
.BI \-j\  num \fR,\ \fB\-\-jobs= num
Allocate \fInum\fR jobs for deme optimization.
Jobs can be executed on a remote machine as well,
in such case the notation \fB\-j\fR \fIN:REMOTE_HOST\fR is used,
where \fIN\fR is the number of jobs on the machine \fIREMOTE_HOST\fR.
For instance, one can enter the options
\fB\-j\fR4 \fB\-j\fR16:my_server.org
(or \fB\-j\fR16:user@my_server.org if one wishes to
run the remote jobs under a different user name),
meaning that 4 jobs are allocated on the local machine
and 16 jobs are allocated on my_server.org.
The assumption is that moses must be on the remote
machine and is located in a directory included in the
\fBPATH\fR environment variable. Beware that a lot of log
files are going to be generated when using this option on
the remote machines.

.\" ============================================================
.SS "Input specification options"
These options control how input data is specified and interpreted.
In its primary mode of operation, \fBmoses\fR performs regression on a
a table of input data. One column is designated as the target, the
remaining columns are taken as predictors.  The output of regression
is a \fBcombo\fR program that is a function of the predictors,
reproducing the target.
.PP
Input files should consist of ASCII data, separated by commas or
whitespace.  The appearance of \fB# ;\fR or \fB!\fR in the first
column denotes a comment. The first non-comment, non-numeric row in the
file (if any) is taken to hold column labels. The target column may be
specified using the \fB\-u\fR option with a column name. The printing of
column names on output is controlled with the \fB\-W1\fR flag.
.TP
.BI \-b\  num \fR,\ \fB\-\-nsamples= num
The number of samples to be taken from the input file. Valid values
run between 1 and the number of rows in the data file; other values
are ignored. If this option is absent, then all data rows are used.
If this option is present, then the input table is sampled randomly
to reach this size.
.TP
.BI \-G\  num \fR,\ \fB\-\-weighted\-accuracy= num
Huh ???
.TP
.BI \-H\  type \fR,\ \fB\-\-problem\-type= type
A number of
.I type
may be one of:
.TS
tab (@);
l lx.
\fBann-it\fR@T{
Regression on an input table, using a neural network.
T}
\fBit\fR@T{
Regression on an input table.
T}
\fBkl\fR@T{
Regression on an input table, by maximizing the Kullback-Leibler
divergence between the distribution of the outputs.  That is, the
output must still be well-scored, but it is assumed that there are
many possible maxima. (XXX ???)
T}
.TE
.TP
.BI \-i\  filename \fR,\ \fB\-\-input\-file= filename
The \fIfilename\fR specifies the input data file. The input table must
be in 'delimiter-separated value' (DSV) format.  Valid seperators 
are comma (CSV, or comma-separated values), blanks and tabs 
(whitespace). Columns correspond to features; there is one sample per
(non-blank) row. Comment characters are hash, bang and semicolon (#!;)
lines starting with a comment are ignored.
The \fB-i\fR flag may be specified multiple times, to indicate multiple
input files. All files must have the same number of columns.
.TP
.BI \-u\  label \fR,\ \fB\-\-target\-feature= label
The \fIlabel\fR is used as the target feature to fit.  If none is
specified, then the first column is used.  The very first row of the
input file, if it contains non-numeric, non-boolean values, is 
interpreted as column labels, as is the common practice for 
CSV/DSV file formats.

.\" ============================================================
.SS "Algorithm control options"
These options provide overall control over the algorithm execution.
The most important of these, for controlling behaviour, are the 
\fB\-a\fR, \fB\-m\fR, \fB\-r\fR \fB\-v\fR and \fB\-z\fR flags.
.TP
.BI \-a\  algorithm \fR,\ \fB\-\-algo= algorithm
Select the algorithm to apply to a single deme.  This is the algorithm
used in the 'inner loop': given a single exemplar decorated with tunable
\fIknobs\fR, this algorithm searches for the best possible knob settings.
Once these are found (or a timeout, or other terminatation condition is
reached), control is returned to the outer optimization loop.
Available algorithms include:
.TS
tab (@);
l lx.
\fBhc\fR@T{
Hillclimbing. There are two primary modes of operation; each has
strengths and weaknesses for different problem types.
In the default mode, one begins with an initial collection of 
knob settings, called an \fIinstance\fR. The settings of each knob is
then varied, in turn, until one setting is found that most improves
the score. This setting then becomes the new instance, and the 
process is repeated, until no further improvement is seen. The 
resulting instance is a local maximum; it is returned to
the outer loop.

The alternate mode of operation is triggered by using the
\fB\-L1\fR flag (usualy with the \fB\-T1\fR flag). In this
case, as before, all knob settings are explored, one knob at a time.
After finding the one knob that most improves the score, the
algo is done, and the resulting instance is returned to the outer
loop. If no knob settings improved the score, then all possible
settings of two knobs are explored, and then three, etc. until
improvement is found (or the alloted iterations are exceeded).
In this alternate mode, the local hill is \fBnot\fR climbed to
the top; instead, any improvement is immediately handed back to the
outer loop, for another round of exemplar selection and knob-building.
For certain types of problems, including maximally misleading problems,
this can arrive at better solutions, more quickly, than the 
traditional hill-climbing algorithm described above.
T}

\fBsa\fR@T{
Simulated annealing.  (Deprated). The \fB\-D\fR flag controls the size 
of the neighborhood that is searched during the early, "high-temperature"
phase.  It has a significant effect on the run-time performance of the
algorithm. Using \fB\-D2\fR or \fB\-D3\fR is likely to provide the best
performance.

The current implementation of this algorithm has numerous faults, making
it unlikely to work well for most problems.
T}

\fBun\fR@T{
Univariate Bayesian dependency.
T}
.TE

.TP
.BI \-A\  score \fR,\ \fB\-\-max\-score= score
Specifies the ideal score for a desired solution; used to terminate
search.  If the maximum number of evaluations has not yet elasped
(set with the \fB\-m\fR option), and a candidate solution is found
that has at least this score, then search is terminated.
.TP
.BI \-B\  effort \fR,\ \fB\-\-reduct\-knob\-building\-effort= effort
Effort allocated for reduction during the knob-building stage.
Valid values are in the range 0-3, with 0 standing for minimum effort,
and 3 for maximum effort. Larger efforts result in demes with fewer
knobs, thus lowering the overall dimension of the problem. This can
improve performance by effectively reducing the size of the problem.
The default \fIeffort\fR is 2.
.TP
.BI \-D dist \fR,\ \fB\-\-max\-dist= dist
The maximum radius of the neighborhood around the exemplar to explore.
The default value is 4.
.TP
.BI \-d1\fR,\ \fB\-\-reduce\-all=1
Reduce candidates before scoring evalutation. Otherwise, only dominating
candidates are reduced, just before being added to the metapopulation.
This flag may be useful if scoring function evaluation expense depends
strongly one the structure of the candidate. It is particularly important
to specify this flage when memoization is enabled (with \fB-s!\fR).
.TP
.BI \-E\  effort \fR,\ \fB\-\-reduct\-candidate\-effort= effort
Effort allocated for reduction of candidates. Valid values are
in the range 0-3, with 0 standing for minimum effort, and 3
for maximum effort. For certain very symmetric problems, such
as the disjunct problem, greater reduction can lead to significantly
faster solution-finding.  The default \fIeffort\fR is 2.
.TP
.BI \-g\  num \fR,\ \fB\-\-max\-gens= num
Create and optimize no more than \fInum\fR demes.  Negative numbers
are interpreted as "unlimited". By default, the number of demes is
unlimited.
.TP
.BI \-I0\fR,\ \fB\-\-include\-dominated=0
Disable the merging of dominated candidates into the metapopulation.
When this flag is specified, the metapopulation will consist entirely
of the highest scoring candidates.  Specifying this flag can (severely)
degrade performance, as this will make it more likely that the
algorithm will get trapped in a local maximum. In addition, culling
the dominated candidates takes a significant amount of CPU time and
complexity.
.TP
.BI \-L1\fR,\ \fB\-\-hc\-single\-step=1
Single-step, instead of hill-climbing to the top of a hill. That is,
a single uphill step is taken, and the resulting best demes are folded
back into the metapopulation.  Solving then continues as usual. By
default, the hillclimbing algorithm does not single-step; it instead
continues to the top of the local hill, before folding the resulting
demes back into the metapopulation.  If using this flag, condiser
usig the \fB\-T1\fR flag to allow the search to be widened, so that
if the initial exemplar is already at the top of a local hill, a search
is made for a different (taller) hill.
.TP
.BI \-m\  num \fR,\ \fB\-\-max\-evals= num
Perform no more than \fInum\fR evaluations of the scoring function.
Default value is 10000.
.TP
.BI \-N\  oper \fR,\ \fB\-\-include\-only\-operator= oper
Include the operator \fIoper\fP, but exclude others, in the solution.
This option may be used several times to specify multiple
operators.  Currently, \fIoper\fP may be one of
\fBplus\fP, \fBtimes\fP, \fBdiv\fP, \fBsin\fP,
\fBexp\fP, \fBlog\fP, \fBimpulse\fP
or a variable \fB#\fP\fIn\fP.
Note that variables and operators are treated separately, so
that including only some operators will still include all
variables, and including only some variables still include
all operators).  You may need to put variables under double
quotes.  This option does not work with ANN.
.TP
.BI \-n\  oper \fR,\ \fB\-\-ignore\-operator= oper
Exclude the operator \fIoper\fP from the program solution.
This option may be used several times.  Currently, \fIoper\fP
may be one of \fBdiv\fP, \fBsin\fP, \fBexp\fP, \fBlog\fP,
\fBimpulse\fP
or a variable \fB#\fP\fIn\fP.
You may need to put variables under double quotes.
This option has the priority over the \-N option.
That is, if an operator is both be included and ignored,
then it is ignored.  This option does not work with ANN.
.TP
.BI \-P\  num \fR,\ \fB\-\-pop\-size\-ratio= num
Controls amount of time spent on a deme. Default value is 20.
.TP
.BI \-r\  seed \fR,\ \fB\-\-random\-seed= seed
Use \fIseed\fR as the seed value for the pseudo-random number generator.
.TP
.BI \-s1\fR,\ \fB\-\-enable\-cache=1
Enable memoization of candidate scores.  This allows the number of scoring
function evaluations to be reduced, by maintaining a cache of recently
scored candidates. If a new candidate is found in the cache, that score
is used, instead of a scoring function evaluation.  The effectiveness of
memoization is greatly increased by also using the \fB\-d1\fR flag.
.TP
.BI \-T1\fR,\ \fB\-\-hc\-widen\-search=1
Controls hill-climbing algorithm behaviour.  If false (the default),
then deme search terminates when a local hilltop is found. If true,
then the search radius is progressively widened, until another
termination condition is met.  Consider using the \fB\-D\fR flag to
set the maximum search radius.
.TP
.BI \-v\  temperature \fR,\ \fB\-\-complexity\-temperature= temperature
Set the "temperature" of the Boltzmann-like distribution used to 
select the next exemplar out of the metapopulaton. A temperature that
is too high or too low will make it likely that poor exemplars will be
chosen for exploration, thus resulting in excessively long search times.
Recommended values lie in the range of 1 to 10, with a default of 4.
.TP
.BI \-z\  ratio \fR,\ \fB\-\-complexity\-ratio= ratio
Fix the ratio of raw score to complexity, when ranking the 
metapopulation for fitness.  Setting this ratio too low causes the 
complexity to dominate ranking, possibly trapping the algorithm in a 
local maximum.  Setting this ratio too high will add too much noise to the 
metapopulation, preventing a solution from being found.  Recommended
values would be in the range from 1 to 5; with 3.5 as the default.

.\" ============================================================
.SS "Output control options"
These options control the displayed output.
.TP
.BI \-C1\fR,\ \fB\-\-output\-dominated=1
Print all of the final metapopulation, and not just the highest-scoring
candidates.
.TP
.BI \-c\  count \fR,\ \fB\-\-result\-count= count
The number of non-dominated (best) results to return, ordered according
to score. If negative, then all results are returned, including the
dominated results.
.TP
.BI \-f\  filename \fR,\ \fB\-\-log\-file= filename
Write debug log traces \fIfilename\fR. If not specified, traces
are written to \fBmoses.log\fR.
.TP
.BI \-F\fR,\ \fB\-\-log\-file\-dep\-opt
Write debug log traces to a filename constructed from the passed
option flags and values. The filename will be truncated to a maximum
of 255 characters.
.TP
.BI \-l\  loglevel \fR,\ \fB\-\-log\-level= loglevel
Specify the level of detail for debug loging. Possible
values for \fIloglevel\fR are \fBNONE\fR, \fBERROR\fR, \fBWARN\fR,
\fBINFO\fR, \fBDEBUG\fR, and \fBFINE\fR. Case does not matter.
Caution: excessive logging detail can lead to significant
program slowdown.
.TP
.BI \-o\  filename \fR,\ \fB\-\-output\-file= filename
Write results to \fIfilename\fR. If not specified, results are written to
\fBstdout\fR.
.TP
.BI \-S0\fR,\ \fB\-\-output\-score=0
Prevent printing of the score.
.TP
.BI \-t1\fR,\ \fB\-\-output\-bscore=1
Print the behavioural score.
.TP
.BI \-V1\fR,\ \fB\-\-output\-eval\-number=1
Print the number of evaluations performed.
.TP
.BI \-W1\fR,\ \fB\-\-output\-with\-labels=1
Use named labels instead of position placeholders when printing
candidates. For example, *("$temperature" "$entropy") instead
of *($3 $4). This option is effective only when the data file
contains labels in its header.
.TP
.BI \-x1\fR,\ \fB\-\-output\-complexity=1
Print the complexity score.

.\" ============================================================
.SS "Contin options"
Options that affect the usage of continuously-valued variables.
.TP
.BI \-q\  num \fR,\ \fB\-\-min\-rand\-input= num
Minimum value for continuous variables. Default 0.0.
.TP
.BI \-R\  num \fR,\ \fB\-\-discretize\-threshold= num
Split a continuous domain into two pieces. This option maybe be used
multiple times to split a continuous domain into multiple peices:
that is, \fIn\fR uses of this option will create \fIn+1\fR domains.
.TP
.BI \-w\  num \fR,\ \fB\-\-max\-rand\-input= num
Maximum value for continuous variables.  Default 1.0.

.\" ============================================================
.SS "Demo options"
These options pertain to the varius built-in demo and example modes.
.TP
.BI \-H\  type \fR,\ \fB\-\-problem\-type= type
A number of demonstration problems are supported. In each case, the top
results are printed to stdout, as a score, followed by a combo program.
.I type
may be one of:
.TS
tab (@);
l lx.
\fBcp\fR@T{
Combo program regression. The scoring function is based on the
combo program specified with the \fB-y\fR flag. That is, the goal of
the run is to deduce and learn the specified combo program.
T}
\fBdj\fR@T{
Disjunction problem. The scoring function awards a result that is a
boolean disjunction (\fIor\fR) of \fIN\fR boolean-valued variables.
The resulting combo program should be \fIor($1 $2 ...)\fR.
The size of the problem may be specified with the \fB\-k\fR option.
T}
\fBmux\fR@T{
Multiplex problem. The scoring function models a boolean digital
multiplexer, that is, an electronic circuit where an "address" of \fIn\fR
bits selects one and only one line, out of \fI2^n\fR possible lines. Thus,
for example, a single address bit can select one of two possible lines:
the first, if its false, and the second, if its true. The \fB\-k\fR
option may be used to specify the value of \fIn\fR.  The actual size
of the problem, measured in bits, is \fIn+2^n\fR and so increases
exponentially fast.
T}
\fBpa\fR@T{
Even parity problem.  The resulting combo program computes the parity of
\fIk\fR bits, evaluating to true if the parity is even, else evaluating
to false.
The size of the problem may be specified with the \fB\-k\fR option.
T}
\fBsr\fR@T{
Polynomial regression problem. Given the polynomial
\fIp(x)=x+x^2+x^3+...x^k\fR, this searches for the shortest program
consisting of nested arithmetic operators to compute \fIp(x)\fR,
given \fIx\fR as a free variable. The arithmetic operators would be
addition, subtraction, multiplication and division; exponentiation
is not allowed in the solution.  So, for example, using the
\fB\-k2\fR option to specify the order\-2 polynomial \fIx+x^2\fR,
then the shortest combo program is \fI*(+(1 $1) $1)\fR (that is,
the solution is \fIp(x)=x(x+1)\fR in the usual arithmetical notation).
T}
.TE

The size of the demonstration problem may be specified with the -k flag.
.TP
.BI \-k\  size \fR,\ \fB\-\-problem\-size= size
Specify the size of the problem.  The interpretation of \fIsize\fR
depends on the particular problem type.
.TP
.BI \-y\  prog \fR,\ \fB\-\-combo\-program= prog
Specify the combo prorgram to be learned, when used in combination with
the \fB-H cp\fR option.  Thus, for example, \fB-H cp -y "and($1 $2)"\fR
specifies that the two-input conjunction is to be learned.
.PP
.\" ============================================================
.SH TODO
Finish documenting these aglo flags:  -M
.PP
These input flags: -b -G
.PP
These operator/problem type flags: -p

.SH SEE ALSO
.br
More information is available at
.B http://wiki.opencog.org/w/MOSES
.SH AUTHORS
.nh
\fBmoses\fP was written by Moshe Looks, Nil Geisweiller, and many others.
.PP
This manual page is being written by Linas Vepstas. It is INCOMPLETE.
