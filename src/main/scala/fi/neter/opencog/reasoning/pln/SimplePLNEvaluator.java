package fi.neter.opencog.reasoning.pln;

import java.util.List;
import java.util.Set;

import fi.neter.opencog.atomspace.Vertex;
import fi.neter.opencog.atomspace.VertexSeq;
import fi.neter.opencog.util.Tree;
import fi.neter.opencog.util.Tree.Iterator;

/**
 * From file PLNEvaluator.h
 * @author tero
 *
 */
public class SimplePLNEvaluator {

	/**
	 * static bool exists(Handle top, Handle* args, const int N, Handle& ret);
	 * @param top
	 * @return
	 */
	public static native long exists(long top, long[] args, int n);
	
	/**
	 * static bool exists(Handle top, const VertexVector& args, Handle& ret);
	 * @param top
	 * @return
	 */
	public static native boolean exists(long top, VertexSeq args);
	 
	/**
	 * static bool exists(Handle top, const std::vector<BoundVertex>& args, Handle& ret);
	 * @param top
	 * @return
	 */
	public static native boolean exists(long top, List<BoundVertex> args);
	
	/**
	 * static bool exists(Handle top, const std::vector<Handle>& args, Handle& ret);
	 * @param h
	 * @return
	 */
	public static native boolean exists(long top);
	
	/**
	 * static bool supportedOperator(Handle h);
	 * @return
	 */
	public static native boolean supportedOperator(long h);
		
	/**
	 * static Handle unify_all(const std::set<BoundVertex>& v);
	 * @return
	 */
	public static native long unifyAll(Set<BoundVertex> v);
		
	/**
	 * static Btr<BV_Set> w_evaluate(const tree<Vertex>& target,
	 *                               tree<Vertex>::iterator top,
	 * 							     MetaProperty policy);
	 */
	public static native BVSet wEvaluate(Tree<Vertex> target,
			Iterator<Tree<Vertex>> top, MetaProperty policy);
}
