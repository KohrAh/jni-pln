#include <jni.h>
#include "../../../include/opencog/atomspace/SimpleTruthValue.h"
#include "../../../include/opencog/atomspace/TruthValue.h"
#include "../../../native_lib/linux/target/jni_pln-javah/jni_pln.h"

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jobject JNICALL Java_fi_neter_opencog_atomspace_SimpleTruthValue_create
  (JNIEnv * env, jclass cls, jfloat mean, jfloat count) {
	SimpleTruthValue* stv = new SimpleTruthValue(mean, count);

	// Creating the Java side wrapper for the created object.
	jobject obj = env->AllocObject(cls);
	jfieldID fid = env->GetFieldID(cls, "handle", "J");
	env->SetLongField(obj, fid, (jlong)stv);
	return obj;
}

#ifdef __cplusplus
}
#endif
