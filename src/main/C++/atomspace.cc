#include <jni.h>
#include "../../../include/opencog/atomspace/AtomSpace.h"
#include "../../../include/opencog/atomspace/TruthValue.h"
#include "../../../native_lib/linux/target/jni_pln-javah/jni_pln.h"

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jobject JNICALL Java_fi_neter_opencog_atomspace_AtomSpace_construct
  (JNIEnv * env, jclass cls) {
	// Creating a new AtomSpace.
	opencog::AtomSpace* as = new opencog::AtomSpace();

	// Creating the Java side wrapper for the created object.
	jobject obj = env->AllocObject(cls);
	jfieldID fid = env->GetFieldID(cls, "handle", "J");
	env->SetLongField(obj, fid, (jlong)as);
	return obj;
}

JNIEXPORT jlong JNICALL Java_fi_neter_opencog_atomspace_AtomSpace_addNode
  (JNIEnv * env, jobject obj, jint t, jstring name, jobject tvObj) {
	// Getting the native object.
	jclass cls = env->GetObjectClass(obj);
	jfieldID fid = env->GetFieldID(cls, "handle", "J");
	opencog::AtomSpace* as = (opencog::AtomSpace*)env->GetLongField(obj, fid);

	jclass tvCls = env->GetObjectClass(tvObj);
	jfieldID tvFid = env->GetFieldID(cls, "handle", "J");
	opencog::TruthValue* truthValue = (opencog::TruthValue*)env->GetLongField(tvObj, tvFid);

    jboolean iscopy;
	std::string nameStr = env->GetStringUTFChars(name, &iscopy);

	opencog::Handle handle = as->addNode(t, nameStr, *truthValue);

	return handle.value();
}



#ifdef __cplusplus
}
#endif
